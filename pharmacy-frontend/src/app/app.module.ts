import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { ReactiveFormsModule } from '@angular/forms';
import { BsDropdownModule } from 'ngx-bootstrap/dropdown';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WorkerComponent } from './worker/worker.component';
import { PharmacistComponent } from './worker/pharmacist/pharmacist.component';
import { ComercialistComponent } from './worker/comercialist/comercialist.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { PharmacyComponent } from './pharmacy/pharmacy.component';
import { PharmacyUpdateComponent } from './pharmacy/pharmacy-update/pharmacy-update.component';
import { PharmacyAddComponent } from './pharmacy/pharmacy-add/pharmacy-add.component';
import { PharmacyListComponent } from './pharmacy/pharmacy-list/pharmacy-list.component';
import { AddPharmacistComponent } from './worker/pharmacist/add-pharmacist/add-pharmacist.component';
import { UpdatePharmacistComponent } from './worker/pharmacist/update-pharmacist/update-pharmacist.component';
import { PharmacyInventoryComponent } from './pharmacy/pharmacyinventory/pharmacyinventory.component';
import { AddMedicineComponent } from './pharmacy/pharmacyinventory/add-medicine/add-medicine.component';
import { UpdateMedicineComponent } from './pharmacy/pharmacyinventory/update-medicine/update-medicine.component';
import { AddComercialistComponent } from './worker/comercialist/add-comercialist/add-comercialist.component';
import { UpdateComercialistComponent } from './worker/comercialist/update-comercialist/update-comercialist.component';
import { SupplierComponent } from './supplier/supplier.component';
import { SupplierListComponent } from './supplier/supplier-list/supplier-list.component';
import { AddSupplierComponent } from './supplier/add-supplier/add-supplier.component';
import { UpdateSupplierComponent } from './supplier/update-supplier/update-supplier.component';
import { SupplierInventoryComponent } from './supplier/supplierinventory/supplierinventory.component';
import { AddMedicineofsupplierComponent } from './supplier/supplierinventory/add-medicineofsupplier/add-medicineofsupplier.component';
import { UpdateMedicineofsupplierComponent } from './supplier/supplierinventory/update-medicineofsupplier/update-medicineofsupplier.component';
import { OrdersComponent } from './supplier/supplierinventory/orders/orders.component';
import { UpdateOrderComponent } from './supplier/supplierinventory/orders/update-order/update-order.component';
import { AddOrderComponent } from './supplier/supplierinventory/orders/add-order/add-order.component';
import { DocumentComponent } from './pharmacy/document/document.component';
import { AddDocumentComponent } from './pharmacy/document/add-document/add-document.component';
import { FindDocumentComponent } from './pharmacy/document/find-document/find-document.component';
import { AddDeliveredComponent } from './pharmacy/document/delivered/add-delivered/add-delivered.component';
import { ViewdeliveredComponent } from './pharmacy/document/delivered/viewdelivered/viewdelivered.component';
import { SaleComponent } from './pharmacy/sale/sale.component';

@NgModule({
  declarations: [
    AppComponent,
    WorkerComponent,
    PharmacistComponent,
    ComercialistComponent,
    HomepageComponent,
    HeaderComponent,
    PharmacyComponent,
    PharmacyUpdateComponent,
    PharmacyAddComponent,
    PharmacyListComponent,
    AddPharmacistComponent,
    UpdatePharmacistComponent,
    AddComercialistComponent,
    UpdateComercialistComponent,
    SupplierComponent,
    SupplierListComponent,
    AddSupplierComponent,
    UpdateSupplierComponent,
    PharmacyInventoryComponent,
    AddMedicineComponent,
    UpdateMedicineComponent,
    SupplierInventoryComponent,
    AddMedicineofsupplierComponent,
    UpdateMedicineofsupplierComponent,
    OrdersComponent,
    UpdateOrderComponent,
    AddOrderComponent,
    DocumentComponent,
    AddDocumentComponent,
    FindDocumentComponent,
    AddDeliveredComponent,
    ViewdeliveredComponent,
    SaleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    HttpClientModule,
    BsDropdownModule.forRoot(),
    BrowserAnimationsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
