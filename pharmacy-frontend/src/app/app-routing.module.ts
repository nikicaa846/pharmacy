import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddDocumentComponent } from './pharmacy/document/add-document/add-document.component';
import { AddDeliveredComponent } from './pharmacy/document/delivered/add-delivered/add-delivered.component';
import { ViewdeliveredComponent } from './pharmacy/document/delivered/viewdelivered/viewdelivered.component';
import { DocumentComponent } from './pharmacy/document/document.component';
import { FindDocumentComponent } from './pharmacy/document/find-document/find-document.component';
import { HomepageComponent } from './homepage/homepage.component';
import { AddMedicineComponent } from './pharmacy/pharmacyinventory/add-medicine/add-medicine.component';
import { PharmacyInventoryComponent } from './pharmacy/pharmacyinventory/pharmacyinventory.component';
import { UpdateMedicineComponent } from './pharmacy/pharmacyinventory/update-medicine/update-medicine.component';
import { PharmacyAddComponent } from './pharmacy/pharmacy-add/pharmacy-add.component';
import { PharmacyListComponent } from './pharmacy/pharmacy-list/pharmacy-list.component';
import { PharmacyUpdateComponent } from './pharmacy/pharmacy-update/pharmacy-update.component';
import { PharmacyComponent } from './pharmacy/pharmacy.component';
import { SaleComponent } from './pharmacy/sale/sale.component';
import { AddSupplierComponent } from './supplier/add-supplier/add-supplier.component';
import { AddMedicineofsupplierComponent } from './supplier/supplierinventory/add-medicineofsupplier/add-medicineofsupplier.component';
import { SupplierInventoryComponent } from './supplier/supplierinventory/supplierinventory.component';
import { AddOrderComponent } from './supplier/supplierinventory/orders/add-order/add-order.component';
import { OrdersComponent } from './supplier/supplierinventory/orders/orders.component';
import { UpdateOrderComponent } from './supplier/supplierinventory/orders/update-order/update-order.component';
import { UpdateMedicineofsupplierComponent } from './supplier/supplierinventory/update-medicineofsupplier/update-medicineofsupplier.component';
import { SupplierListComponent } from './supplier/supplier-list/supplier-list.component';
import { SupplierComponent } from './supplier/supplier.component';
import { UpdateSupplierComponent } from './supplier/update-supplier/update-supplier.component';
import { AddComercialistComponent } from './worker/comercialist/add-comercialist/add-comercialist.component';
import { ComercialistComponent } from './worker/comercialist/comercialist.component';
import { UpdateComercialistComponent } from './worker/comercialist/update-comercialist/update-comercialist.component';
import { AddPharmacistComponent } from './worker/pharmacist/add-pharmacist/add-pharmacist.component';
import { PharmacistComponent } from './worker/pharmacist/pharmacist.component';
import { UpdatePharmacistComponent } from './worker/pharmacist/update-pharmacist/update-pharmacist.component';
import { WorkerComponent } from './worker/worker.component';

const routes: Routes = [
  { path: '', redirectTo: '/homepage', pathMatch: 'full'},
  { path: 'homepage', component: HomepageComponent},
  { path: '',
  component: WorkerComponent,
  children: [
    { path: 'worker', component: WorkerComponent},
    { path: 'pharmacist', component: PharmacistComponent},
    { path: 'comercialist', component: ComercialistComponent},
    { path: 'addpharmacist', component: AddPharmacistComponent},
    { path: 'editpharmacist/:id', component: UpdatePharmacistComponent},
    { path: 'addcomercialist', component: AddComercialistComponent},
    { path: 'editcomercialist/:id', component: UpdateComercialistComponent}
  ]
  },
  { path: '',
    component: PharmacyComponent,
    children: [
      { path: 'pharmacy', component: PharmacyListComponent},
      { path: 'addpharmacy', component: PharmacyAddComponent},
      { path: 'editpharmacy/:id', component: PharmacyUpdateComponent},
      { path: 'pharmacyinventory/:id', component: PharmacyInventoryComponent},
      { path: 'addpharmacymedicine/:id', component: AddMedicineComponent},
      { path: 'editphramcymedicine/:idPharmacy/:idMedicine/:idSupplier', component: UpdateMedicineComponent},
      { path: 'sell/:id', component: SaleComponent}
    ]
  },
  { path: '',
    component: SupplierComponent,
    children: [
      { path: 'supplier', component: SupplierListComponent},
      { path: 'addsupplier', component: AddSupplierComponent},
      { path: 'editsupplier/:id', component: UpdateSupplierComponent},
      { path: 'supplierinventory/:id', component: SupplierInventoryComponent},
      { path: 'addsuppliermedicine/:id', component: AddMedicineofsupplierComponent},
      { path: 'editsuppliermedicine/:idSupplier/:idMedicine', component: UpdateMedicineofsupplierComponent},
      { path: 'orders', component: OrdersComponent},
      { path: 'addorders/:idSupplier/:idMedicine', component: AddOrderComponent},
      { path: 'editorders/:id', component: UpdateOrderComponent}
    ]
  },
  { path: '',
    component: DocumentComponent,
    children: [
      { path: 'pharmacydocument', component: FindDocumentComponent},
      { path: 'adddocument', component: AddDocumentComponent},
      { path: 'pharmacydelivered', component: ViewdeliveredComponent},
      { path: 'adddelivered/:idDocument/:idPharmacy/:idSupplier', component: AddDeliveredComponent},
      { path: 'viewdelivered/:id', component: ViewdeliveredComponent}
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
