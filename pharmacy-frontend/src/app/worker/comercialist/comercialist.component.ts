import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Comercialist } from 'src/app/models/comercialist';
import { Worker } from 'src/app/models/worker';
import { ComercialistService } from 'src/app/services/comercialist.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-comercialist',
  templateUrl: './comercialist.component.html',
  styleUrls: ['./comercialist.component.css']
})
export class ComercialistComponent implements OnInit {

  jmbg: FormControl;
  comercialist: Comercialist = new Comercialist(null, null);
  worker: Worker = new Worker();
  click = false;

  constructor(
    private comercialistService: ComercialistService,
    private workerService: WorkerService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.jmbg = new FormControl('');
  }

  findComercialist(){
    this.click = true;
    this.comercialistService.getComercialistById(this.jmbg.value).subscribe(
      data => {
        this.comercialist = new Comercialist(data.jmbgworker, data.mobilePhone);
      }
    );
    this.workerService.getWorkerById( this.jmbg.value).subscribe(
      data => {
        this.worker = data;
      }
    )
  }

  onUpdate(id: number){
    this.router.navigate(['/editcomercialist', id]);
  }

  onClose(){
    this.click = false;
  }

}
