import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddComercialistComponent } from './add-comercialist.component';

describe('AddComercialistComponent', () => {
  let component: AddComercialistComponent;
  let fixture: ComponentFixture<AddComercialistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddComercialistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddComercialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
