import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Comercialist } from 'src/app/models/comercialist';
import { Worker } from 'src/app/models/worker';
import { ComercialistService } from 'src/app/services/comercialist.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-comercialist',
  templateUrl: './add-comercialist.component.html',
  styleUrls: ['./add-comercialist.component.css']
})
export class AddComercialistComponent implements OnInit {

  workerList: Worker[] = [];
  comercialistFormGroup = new FormGroup({
    'jmbg' : new FormControl(''),
    'name' : new FormControl(''),
    'surname' : new FormControl(''),
    'address' : new FormControl(''),
    'phone': new FormControl('')
  });

  constructor(  
    private comercialistService: ComercialistService,
    private router: Router,
    private workerService: WorkerService
    ) { }

  ngOnInit(): void {
    this.workerService.getAll().subscribe(
      data => {
        this.workerList = data;
      }
    )
  }
  
  onAdd(){
    const worker = this.comercialistFormGroup.value;
    const comercialist = new Comercialist( this.comercialistFormGroup.value['jmbg'], this.comercialistFormGroup.value['phone']);
    if (this.workerList.find(item => this.comercialistFormGroup.value['jmbg'] === item.jmbgworker) === undefined) {
      worker.jmbgworker = this.comercialistFormGroup.value['jmbg'];
      this.workerService.createWorker(worker).subscribe(
        data => {
         console.log(data);
        });
     this.comercialistService.createComercialist(comercialist).subscribe(
       data => {
         console.log(data);
       }
     )

    } else {
      this.comercialistService.createComercialist(comercialist).subscribe(
        data => {
          console.log(data);
        }
      ) 
    }
    this.router.navigate(['/comercialist']);
  }
}
