import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ComercialistComponent } from './comercialist.component';

describe('ComercialistComponent', () => {
  let component: ComercialistComponent;
  let fixture: ComponentFixture<ComercialistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ComercialistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ComercialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
