import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateComercialistComponent } from './update-comercialist.component';

describe('UpdateComercialistComponent', () => {
  let component: UpdateComercialistComponent;
  let fixture: ComponentFixture<UpdateComercialistComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateComercialistComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateComercialistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
