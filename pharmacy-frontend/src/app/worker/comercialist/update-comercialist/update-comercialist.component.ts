import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Comercialist } from 'src/app/models/comercialist';
import { ComercialistService } from 'src/app/services/comercialist.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-update-comercialist',
  templateUrl: './update-comercialist.component.html',
  styleUrls: ['./update-comercialist.component.css']
})
export class UpdateComercialistComponent implements OnInit {

  comercialistFormGroup : FormGroup = new FormGroup({
    'jmbg': new FormControl(''),
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'address': new FormControl(''),
    'phone' : new FormControl('')
  });
  id: number;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private comercialistService: ComercialistService,
    private workerService: WorkerService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.comercialistService.getComercialistById(this.id).subscribe(
      data => {
        const mobilePhone = data.mobilePhone;
        this.workerService.getWorkerById(this.id).subscribe(
          data => {
            this.comercialistFormGroup = new FormGroup({
              'jmbg': new FormControl(data.jmbgworker),
              'name': new FormControl(data.name),
              'surname': new FormControl(data.surname),
              'address': new FormControl(data.address),
              'phone': new FormControl(mobilePhone)
            });
          });
      }
    )
  }

  update(){
    const worker = this.comercialistFormGroup.value;
    const comercialist = new Comercialist(this.comercialistFormGroup.value['jmbg'], this.comercialistFormGroup.value['phone']);
    this.workerService.updateWorker(this.id, worker).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    );
    this.comercialistService.updateComercialist(this.id, comercialist).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    )
    this.router.navigate(['/comercialist']);
  }

}
