import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { Pharmacist } from 'src/app/models/pharmacist';
import { Worker } from 'src/app/models/worker';
import { PharmacistService } from 'src/app/services/pharmacist.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-add-pharmacist',
  templateUrl: './add-pharmacist.component.html',
  styleUrls: ['./add-pharmacist.component.css']
})
export class AddPharmacistComponent implements OnInit {

  workerList: Worker[] = [];
  pharmacistFormGroup = new FormGroup({
    'jmbg' : new FormControl(''),
    'name' : new FormControl(''),
    'surname' : new FormControl(''),
    'address' : new FormControl(''),
    'qualification': new FormControl('')
  });

  constructor(private pharmacistService: PharmacistService,
    private router: Router,
    private workerService: WorkerService
  ) { }

  ngOnInit(): void {
    this.workerService.getAll().subscribe(
      data => {
        this.workerList = data;
      }
    )
  }

  onAdd() {
    const worker = this.pharmacistFormGroup.value;
    const pharmacist = new Pharmacist(this.pharmacistFormGroup.value['jmbg'], this.pharmacistFormGroup.value['qualification']);
   if (this.workerList.find(item => worker.jmbgworker === item.jmbgworker) === undefined) {
      worker.jmbgworker = this.pharmacistFormGroup.value['jmbg'];
      this.workerService.createWorker(worker).subscribe(
        data => {
         console.log(data)
        });
      this.pharmacistService.createPharmacist(pharmacist).subscribe(
        data => {
          console.log(data);
        }
      )

    } else {
      this.pharmacistService.createPharmacist(pharmacist).subscribe(
        data => {
          console.log(data);
        }
       )
    }
    this.router.navigate(['/pharmacist']);
    
  }
}
