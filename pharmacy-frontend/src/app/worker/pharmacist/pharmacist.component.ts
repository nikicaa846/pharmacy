import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Pharmacist } from 'src/app/models/pharmacist';
import { Worker } from 'src/app/models/worker';
import { PharmacistService } from 'src/app/services/pharmacist.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-pharmacist',
  templateUrl: './pharmacist.component.html',
  styleUrls: ['./pharmacist.component.css']
})
export class PharmacistComponent implements OnInit {

  jmbg: FormControl;
  pharmacist: Pharmacist = new Pharmacist(null, null);
  worker: Worker = new Worker();
  click = false;

  constructor( private pharmacistService: PharmacistService , 
               private workerService: WorkerService,
               private router: Router
               ) { }

  ngOnInit(): void {
      this.jmbg = new FormControl('');
  }

  findPharmacist(){
    this.click = true;
    this.pharmacistService.getPharmacistById( this.jmbg.value ).subscribe(
      data => {
        this.pharmacist = new Pharmacist(data.jmbgworker, data.qualification);
      }
    );
    this.workerService.getWorkerById( this.jmbg.value).subscribe(
      data => {
        this.worker = data;
        console.log(data);
      }
    )
 
  }

  onUpdate(id: number){
    this.router.navigate(['/editpharmacist', id]);
  }

  onClose(){
    this.click = false;
  }
}
