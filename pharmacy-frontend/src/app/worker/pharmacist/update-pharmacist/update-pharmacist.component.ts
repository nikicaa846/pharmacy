import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Pharmacist } from 'src/app/models/pharmacist';
import { PharmacistService } from 'src/app/services/pharmacist.service';
import { WorkerService } from 'src/app/services/worker.service';

@Component({
  selector: 'app-update-pharmacist',
  templateUrl: './update-pharmacist.component.html',
  styleUrls: ['./update-pharmacist.component.css']
})
export class UpdatePharmacistComponent implements OnInit {

  pharmacistFormGroup : FormGroup = new FormGroup({
    'jmbg': new FormControl(''),
    'name': new FormControl(''),
    'surname': new FormControl(''),
    'address': new FormControl(''),
    'qualification': new FormControl('')
  });
  id: number;

  constructor(
    private workerService: WorkerService,
    private pharmacistService: PharmacistService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.pharmacistService.getPharmacistById(this.id).subscribe(
      data => {
        const qualification = data.qualification;
        this.workerService.getWorkerById(this.id).subscribe(
          data => {
            this.pharmacistFormGroup = new FormGroup({
              'jmbg': new FormControl(data.jmbgworker),
              'name': new FormControl(data.name),
              'surname': new FormControl(data.surname),
              'address': new FormControl(data.address),
              'qualification': new FormControl(qualification)
            });
          });
      }
    )
  }


  update(){
    const worker = this.pharmacistFormGroup.value;
    const pharmacist = new Pharmacist(this.pharmacistFormGroup.value['jmbg'], this.pharmacistFormGroup.value['qualification']);
    this.workerService.updateWorker(this.id, worker).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    );
    this.pharmacistService.updatePharmacist(this.id, pharmacist).subscribe(
      data => {
       window.location.reload();
      },
      error => console.log(error)
    );
    this.router.navigate(['/pharmacist'])
  }
}
