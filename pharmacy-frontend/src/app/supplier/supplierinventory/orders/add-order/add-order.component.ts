import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Orders } from 'src/app/models/orders';
import { MedicineService } from 'src/app/services/medicine.service';
import { OrderService } from 'src/app/services/orders.service';
import { PharmacyService } from 'src/app/services/pharmacy.service';
import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-add-order',
  templateUrl: './add-order.component.html',
  styleUrls: ['./add-order.component.css']
})
export class AddOrderComponent implements OnInit {

  orderFormGroup: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService,
    private pharmacyService: PharmacyService,
    private supplierService: SupplierService,
    private medicineService: MedicineService
  ) { }

  ngOnInit(): void {
    const idSupplier = this.route.snapshot.params['idSupplier'];
    const idMedicine = this.route.snapshot.params['idMedicine'];
    this.orderFormGroup = new FormGroup({
      'idSupplier': new FormControl(idSupplier),
      'idMedicine': new FormControl(idMedicine),
      'idPharmacy': new FormControl(''),
      'amount': new FormControl('')
    });
  }

  onOrder(){
    this.pharmacyService.getPharmacyById(this.orderFormGroup.value['idPharmacy']).subscribe(
      data => {
        const pharmacy = data;
        this.supplierService.getSupplier(this.orderFormGroup.value['idSupplier']).subscribe(
          data => {
            const supplier = data;
            this.medicineService.getMedicineById(this.orderFormGroup.value['idMedicine']).subscribe(
              data => {
                const medicine = data;
                const order = new Orders(medicine, supplier, pharmacy, this.orderFormGroup.value['amount']);
                this.orderService.createOrder(order)
                .subscribe(
                  data => {
                    window.location.reload();
                  }
                );
              }
            )
          }
        )
    })
    this.router.navigate(['orders']);
  }
}
