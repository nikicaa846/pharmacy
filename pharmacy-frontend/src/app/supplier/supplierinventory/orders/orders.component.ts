import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Router } from '@angular/router';

import { Orders } from 'src/app/models/orders';
import { OrderService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.css']
})
export class OrdersComponent implements OnInit {

  orders: Orders[] = [];
  date: FormControl = new FormControl('');
  click = false;

  constructor(
    private orderService: OrderService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  find(){
    this.click = true;
    this.orderService.getOrderByDate(this.date.value).subscribe(
      data => {
        this.orders = data;
        console.log(data);
      }
    );
  }

  onUpdate(id: number){
    this.router.navigate(['/editorders/', id]);
  }

  onCancle(id: number){
    this.orderService.deleteOrder(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }

  onClose(){
    this.click = false;
  }
}
