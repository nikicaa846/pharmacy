import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { OrderService } from 'src/app/services/orders.service';

@Component({
  selector: 'app-update-order',
  templateUrl: './update-order.component.html',
  styleUrls: ['./update-order.component.css']
})
export class UpdateOrderComponent implements OnInit {

  orderFormGroup: FormGroup;
  idorder: number

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private orderService: OrderService
  ) { }

  ngOnInit(): void {
    this.idorder = this.route.snapshot.params['id'];
    this.orderFormGroup = new FormGroup({
      'idSupplier': new FormControl(''),
      'idMedicine': new FormControl(''),
      'idPharmacy': new FormControl(''),
      'amount': new FormControl('')
    });
    this.orderService.getOrderById(this.idorder).subscribe(
      data =>{
        this.orderFormGroup = new FormGroup({
          'idSupplier': new FormControl(data.supplier.idSupplier),
          'idMedicine': new FormControl(data.medicine.idMedicine),
          'idPharmacy': new FormControl(data.pharmacy.idPharmacy),
          'amount': new FormControl(data.amount)
        });
      }
    )
  }

  update(){
    const order = this.orderFormGroup.value;
    this.orderService.updateOrder(this.idorder, order).subscribe(
      data => {
        window.location.reload();
      }
    );
    this.router.navigate(['/orders']);
  }

}
