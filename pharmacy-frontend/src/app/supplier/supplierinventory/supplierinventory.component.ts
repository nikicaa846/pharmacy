import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SupplierInventory } from 'src/app/models/supplierinventory';
import { SupplierInventoryService } from 'src/app/services/supplierinventory.service';

@Component({
  selector: 'app-supplierinventory',
  templateUrl: './supplierinventory.component.html',
  styleUrls: ['./supplierinventory.component.css']
})
export class SupplierInventoryComponent implements OnInit {

  medicines: SupplierInventory[] = [];
  idpharmacy = new FormControl('');

  constructor(
    private supplierinventoryService: SupplierInventoryService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.supplierinventoryService.findByIdSupplier(id).subscribe(
      data => {
        const suppliersMedicines = data;
        for( let i = 0; i < suppliersMedicines.length; i++ ){
          if( suppliersMedicines[i].idSupplier == id){
            this.medicines.push(suppliersMedicines[i]);
          }
        }
      }
    );
  }

  onAdd(id: number){
    this.router.navigate(['/addsuppliermedicine', id]);
  }

  onUpdate(idSupplier: number, idMedicine: number){
    this.router.navigate(["/editsuppliermedicine", idSupplier, idMedicine]);
  }

  onDelete(idSupplier: number, idMedicine: number){
    this.supplierinventoryService.deleteSupplierMedicine(idSupplier, idMedicine).subscribe(
      data => {
        window.location.reload();
      }
    )
  }

  onOrder(idSupplier: number, idMedicine: number){
    this.router.navigate(['addorders', idSupplier, idMedicine]);
  }
}
