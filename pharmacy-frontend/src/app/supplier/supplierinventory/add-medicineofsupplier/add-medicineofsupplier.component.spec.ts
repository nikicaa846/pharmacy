import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddMedicineofsupplierComponent } from './add-medicineofsupplier.component';

describe('AddMedicineofsupplierComponent', () => {
  let component: AddMedicineofsupplierComponent;
  let fixture: ComponentFixture<AddMedicineofsupplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddMedicineofsupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddMedicineofsupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
