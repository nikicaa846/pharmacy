import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Medicine } from 'src/app/models/medicine';
import { SupplierInventoryService} from 'src/app/services/supplierinventory.service';
import { MedicineService } from 'src/app/services/medicine.service';

@Component({
  selector: 'app-add-medicineofsupplier',
  templateUrl: './add-medicineofsupplier.component.html',
  styleUrls: ['./add-medicineofsupplier.component.css']
})
export class AddMedicineofsupplierComponent implements OnInit {

  supplierInventoryFormGroup: FormGroup;
  id: number;

  constructor(
    private supplierinventoryService: SupplierInventoryService,
    private route: ActivatedRoute,
    private router: Router,
    private medicineService: MedicineService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.supplierInventoryFormGroup = new FormGroup({
      'idSupplier' : new FormControl(this.id),
      'idMedicine': new FormControl(''),
      'name' : new FormControl(''),
      'amount' : new FormControl(''),
      'price' : new FormControl('')
    });
  }

  add(){
    const content = this.supplierInventoryFormGroup.value;
    const medicine = new Medicine(this.supplierInventoryFormGroup.value['idMedicine'], this.supplierInventoryFormGroup.value['name']);
    this.supplierinventoryService.createSupplierMedicine(content).subscribe(
      data =>{
        console.log(data);
      }
    );
    this.medicineService.createMedicine(medicine).subscribe(
      data => {
        console.log(data);
        window.location.reload();
      }
    )
    this.router.navigate(['/supplierinventory', this.id]);
  }
}
