import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SupplierInventoryComponent } from './supplierinventory.component';

describe('ContainsComponent', () => {
  let component: SupplierInventoryComponent;
  let fixture: ComponentFixture<SupplierInventoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SupplierInventoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SupplierInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
