import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateMedicineofsupplierComponent } from './update-medicineofsupplier.component';

describe('UpdateMedicineofsupplierComponent', () => {
  let component: UpdateMedicineofsupplierComponent;
  let fixture: ComponentFixture<UpdateMedicineofsupplierComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UpdateMedicineofsupplierComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateMedicineofsupplierComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
