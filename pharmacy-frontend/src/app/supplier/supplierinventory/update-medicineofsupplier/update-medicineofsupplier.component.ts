import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SupplierInventoryService } from 'src/app/services/supplierinventory.service';

@Component({
  selector: 'app-update-medicineofsupplier',
  templateUrl: './update-medicineofsupplier.component.html',
  styleUrls: ['./update-medicineofsupplier.component.css']
})
export class UpdateMedicineofsupplierComponent implements OnInit {

  supplierInventoryFormGroup: FormGroup;
  idsupplier: number;
  idmedicine: number;


  constructor(
    private supplierinventoryService: SupplierInventoryService,
    private route: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.supplierInventoryFormGroup = new FormGroup({
      'idSupplier' : new FormControl(''),
      'idMedicine': new FormControl(''),
      'name' : new FormControl(''),
      'amount' : new FormControl(''),
      'price' : new FormControl('')
    });
    this.idsupplier = this.route.snapshot.params['idSupplier'];
    this.idmedicine = this.route.snapshot.params['idMedicine'];
    this.supplierinventoryService.getSupplierMedicineById(this.idsupplier, this.idmedicine).subscribe(
      data => {
        this.supplierInventoryFormGroup = new FormGroup({
          'idSupplier' : new FormControl(data.idSupplier),
          'idMedicine': new FormControl(data.idMedicine),
          'name' : new FormControl(data.name),
          'amount' : new FormControl(data.amount),
          'price' : new FormControl(data.price)
        });
      }
    )
  }

  update(){
    const medicine = this.supplierInventoryFormGroup.value;
    this.supplierinventoryService.updateSupplierMedicine(this.idsupplier, this.idmedicine, medicine).subscribe(
      data => {
        window.location.reload();
      }
    );
    this.router.navigate(['/supplierinventory', this.idsupplier]);
  }
}
