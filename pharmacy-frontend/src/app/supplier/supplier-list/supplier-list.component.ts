import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Supplier } from 'src/app/models/supplier';
import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-supplier-list',
  templateUrl: './supplier-list.component.html',
  styleUrls: ['./supplier-list.component.css']
})
export class SupplierListComponent implements OnInit {

  supplierlist: Supplier[] = []; 

  constructor( private supplierService: SupplierService, private router: Router ) { }

  ngOnInit(): void {
     this.getSupplierList();
  }

  getSupplierList(){
    this.supplierService.getAll().subscribe(
      data => {
        this.supplierlist = data;
      }
    )
  }

  onUpdate(id: number){
    this.router.navigate(['/editsupplier', id]);
  }

  onDelete(id: number){
    this.supplierService.deleteSupplier(id).subscribe(
      data => {
        window.location.reload();
      }
    )
  }

  onView(id: number){
    this.router.navigate(['/supplierinventory', id]);
  }
}
