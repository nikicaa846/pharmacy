import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-update-supplier',
  templateUrl: './update-supplier.component.html',
  styleUrls: ['./update-supplier.component.css']
})
export class UpdateSupplierComponent implements OnInit {

  
  supplierFormGroup: FormGroup = new FormGroup({
    'id': new FormControl(''),
    'name': new FormControl(''),
    'address' : new FormControl(''),
    'phone' : new FormControl('')
  });

  id: number;

  constructor( 
    private supplierService: SupplierService, 
    private router: Router, 
    private route: ActivatedRoute
    ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.supplierService.getSupplier(this.id).subscribe(
      data =>{
        this.supplierFormGroup = new FormGroup({
          'id': new FormControl(data.idSupplier),
          'name': new FormControl(data.name),
          'address' : new FormControl(data.address),
          'phone' : new FormControl(data.phone)
        });
      }
    )
  }

  update(){
    const supplier = this.supplierFormGroup.value;
    this.supplierService.updateSupplier(this.id, supplier).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    );

    this.router.navigate(['/supplier']);
  }

}
