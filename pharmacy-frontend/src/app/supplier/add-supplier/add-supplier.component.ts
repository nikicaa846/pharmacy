import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-add-supplier',
  templateUrl: './add-supplier.component.html',
  styleUrls: ['./add-supplier.component.css']
})
export class AddSupplierComponent implements OnInit {

  supplierFormGroup: FormGroup;

  constructor( private supplierService: SupplierService, private router: Router) { }

  ngOnInit(): void {
    this.supplierFormGroup = new FormGroup({
      'name': new FormControl(''),
      'address' : new FormControl(''),
      'phone' : new FormControl('')
    })
  }

  onAdd(){
    const supplier = this.supplierFormGroup.value;
    this.supplierService.createSupplier(supplier).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    );

    this.router.navigate(['/supplier']);
  }
}
