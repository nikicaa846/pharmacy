import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pharmacy } from '../models/pharmacy';
import { PharmacyService } from '../services/pharmacy.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  pharmacies: Pharmacy[] = [];

  constructor(
    private pharmacyService: PharmacyService,
    private router: Router
  ){}

  ngOnInit(): void {
    this.pharmacyService.getAll().subscribe(
      data => {
        this.pharmacies = data;
      }
    )
  }

  click(id: number){
    this.router.navigate(['sell', id]);
  }
}
