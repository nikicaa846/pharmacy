import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { SupplierInventory } from '../models/supplierinventory';

@Injectable({providedIn: 'root'})
export class SupplierInventoryService {

    url = 'http://localhost:8080/api/v1/supplierinventory';

    constructor( private http: HttpClient){}

   findByIdSupplier(ids: number): Observable<SupplierInventory[]> {
        return this.http.get<SupplierInventory[]>(this.url + '?idSupplier=' + ids);
    }

    getSupplierMedicineById( ids: number, idm: number) : Observable<SupplierInventory>{ 
        return this.http.get<SupplierInventory>(this.url + '/supplier/'  + ids + '/medicine/' + idm);
    }

    createSupplierMedicine( medicine: SupplierInventory) : Observable<Object> {
        return this.http.post(this.url, medicine);
    }

    updateSupplierMedicine( ids: number, idm: number, medicine: SupplierInventory ) : Observable<Object> {
        return this.http.put(this.url + '/supplier/' + ids + '/medicine/' + idm, medicine);
    }

    sellMedicine( ids: number, idm: number, medicine:SupplierInventory ) : Observable<Object> {
        return this.http.put(this.url + '/sell/supplier/' + ids + '/medicine/' + idm , medicine);
    }

    deleteSupplierMedicine( ids : number, idm: number ) {
        return this.http.delete(this.url + '/supplier/' + ids + '/medicine/' + idm);
    }
}