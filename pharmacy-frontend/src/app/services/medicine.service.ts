import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Medicine } from '../models/medicine';

@Injectable({providedIn: 'root'})
export class MedicineService {

    url = 'http://localhost:8080/api/v1/medicine';

    constructor( private http: HttpClient){}

    getMedicineById( idm: number) : Observable<Medicine>{ 
        return this.http.get<Medicine>(this.url + '/' + idm);
    }

    createMedicine( medicine: Medicine) : Observable<Object> {
        return this.http.post(this.url, medicine);
    }
}