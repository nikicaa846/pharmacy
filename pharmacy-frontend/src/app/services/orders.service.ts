import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { Orders } from '../models/orders';

@Injectable({providedIn: 'root'})
export class OrderService {

    url = 'http://localhost:8080/api/v1/orders';

    constructor( private http: HttpClient){}

    getOrderByDate(date: Date) : Observable<Orders[]>{ 
        return this.http.get<Orders[]>(this.url + '?date=' + date);
    }

    getOrderById(id: number) : Observable<Orders>{ 
        return this.http.get<Orders>(this.url + '/' + id);
    }

    createOrder( order: Orders): Observable<Object> {
        return this.http.post(this.url, order);
    }

    updateOrder( id: number, medicine: Orders ) : Observable<Object> {
        return this.http.put(this.url + '/' + id, medicine);
    }

    deleteOrder( id: number ) {
        return this.http.delete(this.url + '/' + id);
    }
}