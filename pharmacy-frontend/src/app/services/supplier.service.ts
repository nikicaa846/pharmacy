import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Supplier } from '../models/supplier';

@Injectable({providedIn:'root'})
export class SupplierService {

    url = "http://localhost:8080/api/v1/supplier";

    constructor(private http: HttpClient){}

    getAll(): Observable<Supplier[]>{
        return this.http.get<Supplier[]>(this.url);
    }

    getSupplier(id: number): Observable<Supplier> {
        return this.http.get<Supplier>(this.url + '/' + id);
    }

    createSupplier(supplier: Supplier): Observable<Object>{
        return this.http.post(this.url, supplier);
    }

    updateSupplier(id: number, supplier: Supplier): Observable<Object> {
        return this.http.put(this.url + '/' + id, supplier);
    }

    deleteSupplier(id: number){
        return this.http.delete(this.url + '/' + id);
    }
}