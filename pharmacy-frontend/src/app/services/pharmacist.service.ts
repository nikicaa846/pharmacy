import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { Pharmacist } from '../models/pharmacist';

@Injectable({providedIn: 'root'})
export class PharmacistService {

    url = 'http://localhost:8080/api/v1/pharmacist';

    constructor( private http: HttpClient){}

    getPharmacistById(id: number) : Observable<Pharmacist>{ 
        return this.http.get<Pharmacist>(this.url + '/' + id);
    }

    createPharmacist( pharmacist : Pharmacist) : Observable<Object> {
        return this.http.post(this.url, pharmacist);
    }

    updatePharmacist( id : number, pharmacist : Pharmacist) : Observable<Object> {
        return this.http.put(this.url + '/' + id, pharmacist);
    }

    deletePharmacist( id: number ) {
        return this.http.delete(this.url + '/' + id);
    }
}