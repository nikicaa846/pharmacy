import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { Sale } from '../models/sale';

@Injectable({providedIn: 'root'})
export class SaleService {

    url = 'http://localhost:8080/api/v1/sell';

    constructor( private http: HttpClient){}

    getByDate(date: Date) : Observable<Sale[]>{ 
        return this.http.get<Sale[]>(this.url + '?date=' + date);
    }

    addSell( sell: Sale ): Observable<Object> {
        return this.http.post(this.url, sell);
    }

}