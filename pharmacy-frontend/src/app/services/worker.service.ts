import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { Worker } from '../models/worker';


@Injectable({providedIn: 'root'})
export class WorkerService {

    url = 'http://localhost:8080/api/v1/worker';

    constructor( private http: HttpClient){}


    getAll(): Observable<Worker[]> {
        return this.http.get<Worker[]>(this.url);
    }

    getWorkerById(id: number) : Observable<Worker>{ 
        return this.http.get<Worker>(this.url + '/' + id);
    }

    createWorker( worker : Worker) : Observable<Object> {
        return this.http.post(this.url, worker);
    }

    updateWorker( id : number, worker : Worker ) : Observable<Object> {
        return this.http.put(this.url + '/' + id, worker);
    }

    deleteWorker( id: number ) {
        return this.http.delete(this.url + '/' + id);
    }
}