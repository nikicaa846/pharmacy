import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';

import { Pharmacy } from '../models/pharmacy';

@Injectable({providedIn: 'root'})
export class PharmacyService {

    url = "http://localhost:8080/api/v1/pharmacy";

    constructor( private http: HttpClient ) {}

    getAll(): Observable<Pharmacy[]> {
        return this.http.get<Pharmacy[]>(this.url);
    }

    getPharmacyById( id : number): Observable<Pharmacy>{
        return this.http.get<Pharmacy>(this.url + '/' + id);
    }

    addPharmacy(pharmacy : Pharmacy): Observable<Object>{
        return this.http.post(this.url, pharmacy);
    }

    updatePharmacy(id: number, pharmacy : Pharmacy): Observable<Object>{
        return this.http.put(this.url + '/' + id, pharmacy)
    }

    deletePharmacy( id: number ){
        return this.http.delete(this.url + '/' + id);
    }
}