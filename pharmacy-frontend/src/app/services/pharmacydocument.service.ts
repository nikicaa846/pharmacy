import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PharmacyDocument } from '../models/pharmacydocument';

@Injectable({providedIn:'root'})
export class PharmacyDocumentService {

    constructor( private http: HttpClient){}

    url = 'http://localhost:8080/api/v1/pharmacydocument';

    getDocumentByDate(date: Date) : Observable<PharmacyDocument[]>{ 
        return this.http.get<PharmacyDocument[]>(this.url + '?date='+ date);
    }

    getDocumentById(id: number) : Observable<PharmacyDocument>{ 
        return this.http.get<PharmacyDocument>(this.url + '/' + id);
    }

    createDocument(document: PharmacyDocument): Observable<Object>{
        return this.http.post(this.url, document);
    }
}