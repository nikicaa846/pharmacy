import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';

import { Comercialist } from '../models/comercialist';


@Injectable({providedIn: 'root'})
export class ComercialistService {

    url = 'http://localhost:8080/api/v1/comercialist';

    constructor( private http: HttpClient ){}

    getComercialistById(id: number) : Observable<Comercialist>{ 
        return this.http.get<Comercialist>(this.url + '/' + id);
    }

    createComercialist( comercialist : Comercialist) : Observable<Object> {
        return this.http.post(this.url, comercialist);
    }

    updateComercialist( id : number, comercialist : Comercialist ) : Observable<Object> {
        return this.http.put(this.url + '/' + id, comercialist);
    }

    deleteComercialist( id: number ) {
        return this.http.delete(this.url + '/' +id);
    }
}