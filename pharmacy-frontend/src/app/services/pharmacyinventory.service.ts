import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { Observable } from 'rxjs';
import { PharmacyInventory } from '../models/pharmacyinventory';



@Injectable({providedIn: 'root'})
export class PharmacyInventoryService {

    url = 'http://localhost:8080/api/v1/pharmacyinventory';

    constructor( private http: HttpClient){}

    findByPharmacyId(idp: number): Observable<PharmacyInventory[]> {
        return this.http.get<PharmacyInventory[]>(this.url + '?idPharmacy=' + idp);
    }

    getPharmacyMedicineById(idp: number, idm: number, ids: number) : Observable<PharmacyInventory>{ 
        return this.http.get<PharmacyInventory>(this.url + '/pharmacy/' + idp + '/medicine/' + idm + '/supplier/' + ids);
    }

    createPharmacyMedicine( medicine: PharmacyInventory) : Observable<Object> {
        return this.http.post(this.url, medicine);
    }

    updatePharmacyMedicine( idp : number, idm: number, ids: number, medicine:PharmacyInventory ) : Observable<Object> {
        return this.http.put(this.url + '/pharmacy/' + idp + '/medicine/' + idm + '/supplier/' + ids, medicine);
    }

    sellMedicine( idp : number, idm: number, ids: number, medicine: PharmacyInventory ) : Observable<Object> {
        return this.http.put(this.url + '/sell/pharmacy/' + idp + '/medicine/' + idm + '/supplier/' + ids, medicine);
    }

    deliveredMedicine(idp : number, idm: number, ids: number, medicine: PharmacyInventory ) : Observable<Object> {
        return this.http.put(this.url + '/delivered/pharmacy/' + idp + '/medicine/' + idm + '/supplier/' + ids, medicine);
    }
    deletePharmacyMedicine( idp : number, idm: number, ids: number ) {
        return this.http.delete(this.url + '/pharmacy/' + idp + '/medicine/' + idm + '/supplier/' + ids);
    }
}