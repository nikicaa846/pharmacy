import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { PharmacyDelivered } from '../models/pharmacydelivered';

@Injectable({providedIn:'root'})
export class PharmacyDeliveredService {

    constructor( private http: HttpClient){}

    url = 'http://localhost:8080/api/v1/pharmacydelivered';

    findByIdDocument(idd: number): Observable<PharmacyDelivered[]> {
        return this.http.get<PharmacyDelivered[]>(this.url + '?idDocument=' + idd);
    }

    addDelivery(delivery: PharmacyDelivered): Observable<Object>{
        return this.http.post(this.url, delivery);
    }
}