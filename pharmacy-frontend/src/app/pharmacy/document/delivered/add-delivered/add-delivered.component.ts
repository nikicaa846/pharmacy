import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { SupplierInventory } from 'src/app/models/supplierinventory';
import { PharmacyDelivered } from 'src/app/models/pharmacydelivered';
import { PharmacyInventory } from 'src/app/models/pharmacyinventory';
import { Medicine } from 'src/app/models/medicine';
import { SupplierInventoryService } from 'src/app/services/supplierinventory.service';
import { PharmacyDeliveredService } from 'src/app/services/pharmacydelivered.service';
import { PharmacyInventoryService } from 'src/app/services/pharmacyinventory.service';
import { PharmacyDocumentService } from 'src/app/services/pharmacydocument.service';
import { MedicineService } from 'src/app/services/medicine.service';

@Component({
  selector: 'app-add-delivered',
  templateUrl: './add-delivered.component.html',
  styleUrls: ['./add-delivered.component.css']
})
export class AddDeliveredComponent implements OnInit {

  deliveryFormGroup: FormGroup;
  iddocument: number;
  idsupplier: number;
  idpharmacy: number;
  medicine: Medicine;
  availableMedicine: PharmacyInventory;

  constructor(
    private deliveredService: PharmacyDeliveredService,
    private documentService: PharmacyDocumentService,
    private medicineService: MedicineService,
    private pharmacyinventoryService: PharmacyInventoryService,
    private supplierinventoryService: SupplierInventoryService,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.iddocument = this.route.snapshot.params['idDocument'];
    this.idpharmacy = this.route.snapshot.params['idPharmacy'];
    this.idsupplier = this.route.snapshot.params['idSupplier']
    this.deliveryFormGroup = new FormGroup({
      'document' : new FormControl(this.iddocument),
      'idMedicine': new FormControl(''),
      'amount': new FormControl('')
    });
  }

  private deliverMedicine(){
    this.pharmacyinventoryService.findByPharmacyId(this.idpharmacy).subscribe(
      data => {
        const pharmacyMedicines = data;
        if ( pharmacyMedicines.find( item => item.idMedicine === this.medicine.idMedicine) === undefined){
          this.supplierinventoryService.getSupplierMedicineById( this.idsupplier, this.medicine.idMedicine).subscribe(
            data => {
              const price = data.price + data.price * 0.2;
              this.availableMedicine = 
              new PharmacyInventory(this.idpharmacy, data.idMedicine, data.idSupplier, data.name, this.deliveryFormGroup.value['amount'],price);
              this.pharmacyinventoryService.createPharmacyMedicine(this.availableMedicine).subscribe(
                data => {
                  console.log(data);
                }
              )
            }
          )
        } else {
          this.availableMedicine.amount =this.deliveryFormGroup.value['amount'];
          this.pharmacyinventoryService.deliveredMedicine( this.idpharmacy, this.medicine.idMedicine, this.idsupplier, this.availableMedicine)
          .subscribe(
            data => {
              console.log(data);
            }
          )
        }
      }
    )
  }

  private sellSupplierMedicine(){
    const content = new SupplierInventory(this.idsupplier, this.medicine.idMedicine, this.deliveryFormGroup.value['amount']);
    this.supplierinventoryService.sellMedicine( this.idsupplier, this.medicine.idMedicine, content).subscribe(
      data => {
        console.log(data);
      }
    )
  }

  onAdd() {
    this.documentService.getDocumentById(this.iddocument).subscribe(
      date => {
        const document = date;
        this.medicineService.getMedicineById(this.deliveryFormGroup.value['idMedicine']).subscribe(
          data => {
            this.medicine = data;
            const delivery = new PharmacyDelivered( document, this.medicine, this.deliveryFormGroup.value['amount']);
            this.deliveredService.addDelivery(delivery).subscribe(
              data => {
                console.log(data);
              }
            )
            this.deliverMedicine();
            this.sellSupplierMedicine();
          }
        )
      }
    );
  }
}
