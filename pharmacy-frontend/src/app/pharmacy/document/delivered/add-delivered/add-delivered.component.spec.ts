import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDeliveredComponent } from './add-delivered.component';

describe('AddDeliveredComponent', () => {
  let component: AddDeliveredComponent;
  let fixture: ComponentFixture<AddDeliveredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDeliveredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDeliveredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
