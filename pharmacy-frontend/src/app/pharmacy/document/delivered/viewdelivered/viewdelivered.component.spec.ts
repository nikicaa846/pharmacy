import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewdeliveredComponent } from './viewdelivered.component';

describe('ViewdeliveredComponent', () => {
  let component: ViewdeliveredComponent;
  let fixture: ComponentFixture<ViewdeliveredComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewdeliveredComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewdeliveredComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
