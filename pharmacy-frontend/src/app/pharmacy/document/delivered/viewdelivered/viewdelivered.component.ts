import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PharmacyDelivered } from 'src/app/models/pharmacydelivered';
import { PharmacyDocument } from 'src/app/models/pharmacydocument';
import { Pharmacy } from 'src/app/models/pharmacy';
import { Supplier } from 'src/app/models/supplier';
import { PharmacyDeliveredService } from 'src/app/services/pharmacydelivered.service';
import { PharmacyDocumentService } from 'src/app/services/pharmacydocument.service';
import { PharmacyService } from 'src/app/services/pharmacy.service';
import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-viewdelivered',
  templateUrl: './viewdelivered.component.html',
  styleUrls: ['./viewdelivered.component.css']
})
export class ViewdeliveredComponent implements OnInit {

  idDocument: number;
  deliveries: PharmacyDelivered[] = [];
  document: PharmacyDocument = new PharmacyDocument(null, null, null, null);
  pharmacy: Pharmacy = new Pharmacy();
  supplier: Supplier = new Supplier();

  constructor(
    private route: ActivatedRoute,
    private deliveredService: PharmacyDeliveredService,
    private pharmacyService: PharmacyService,
    private supplierService: SupplierService,
    private documentService: PharmacyDocumentService
  ) { }

  ngOnInit(): void {
    this.idDocument = this.route.snapshot.params['id'];
    this.getAllDelivereds();
    this.getDocument();
  }

  getAllDelivereds(){
    this.deliveredService.findByIdDocument(this.idDocument).subscribe(
      data => {
        this.deliveries = data;
      }
    )
  }

  getDocument(){
      this.documentService.getDocumentById(this.idDocument).subscribe(
      data => {
        this.document = data;
        this.pharmacyService.getPharmacyById(this.document.pharmacy.idPharmacy).subscribe(
          data => {
            this.pharmacy = data;
          }
        );
        this.supplierService.getSupplier(this.document.supplier.idSupplier).subscribe(
          data => {
            this.supplier = data;
          }
        )
      }
    )
  }
}
