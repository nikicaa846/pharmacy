import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Comercialist } from 'src/app/models/comercialist';
import { PharmacyDocument } from 'src/app/models/pharmacydocument';
import { Pharmacy } from 'src/app/models/pharmacy';
import { Supplier } from 'src/app/models/supplier';
import { ComercialistService } from 'src/app/services/comercialist.service';
import { PharmacyDocumentService } from 'src/app/services/pharmacydocument.service';
import { PharmacyService } from 'src/app/services/pharmacy.service';
import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-add-document',
  templateUrl: './add-document.component.html',
  styleUrls: ['./add-document.component.css']
})
export class AddDocumentComponent implements OnInit {

  documentFormGroup: FormGroup;
  pharamcies: Pharmacy[] = [];
  supplieries: Supplier[] = [];
  idpharmacy: number;
  idsupplier: number;

  constructor(
    private documentService: PharmacyDocumentService,
    private comercialistService: ComercialistService,
    private pharmacyService: PharmacyService,
    private supplierService: SupplierService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.documentFormGroup = new FormGroup({
      'date': new FormControl(''),
      'jmbgworker': new FormControl(''),
      'supplier': new FormControl(''),
      'pharmacy': new FormControl('')
    })
    this.findPharmacies();
    this.findSuppliers();
  }

  private findPharmacies() {
    this.pharmacyService.getAll().subscribe(
      data => {
        this.pharamcies = data;
      }
    )
  }

  private findSuppliers() {
    this.supplierService.getAll().subscribe(
      data => {
        this.supplieries = data;
      }
    )
  }

  private getIdOfPharmacy(): number {
    if (this.pharamcies.find(item => item.name === this.documentFormGroup.value['pharmacy']) !== undefined) {
      this.idpharmacy = this.pharamcies.find(item => item.name === this.documentFormGroup.value['pharmacy']).idPharmacy;
      return this.idpharmacy;
    }
    return null;
  }

  private getIdOfSupplier(): number {
    if (this.supplieries.find(item => item.name === this.documentFormGroup.value['supplier']) !== undefined) {
      this.idsupplier = this.supplieries.find(item => item.name === this.documentFormGroup.value['supplier']).idSupplier;
      return this.idsupplier;
    }
    return null;
  }

  onAdd() {
    this.comercialistService.getComercialistById(this.documentFormGroup.value['jmbgworker']).subscribe(
      data => {
        const comercialist = data;
        this.getIdOfPharmacy();
        if (this.idpharmacy !== null) {
          this.pharmacyService.getPharmacyById(this.idpharmacy).subscribe(
            data => {
              const pharmacy = data;
              this.getIdOfSupplier();
              if (this.idsupplier !== null) {
                this.supplierService.getSupplier(this.idsupplier).subscribe(
                  data => {
                    const supplier = data;
                    const document = new PharmacyDocument(new Date(this.documentFormGroup.value['date']), comercialist, supplier, pharmacy);
                    this.documentService.createDocument(document)
                      .subscribe(
                        data => {
                          this.router.navigate(['pharmacydocument']);
                        }
                      )
                  }
                );
              } else {
                window.alert("Supplier with name you input doesn't exist!");
              }
            }
          );
        } else {
          window.alert("Pharmacy with name you input doesn't exist!.");
        }
      }
    )
  }
}
