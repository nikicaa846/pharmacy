import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { PharmacyDocument } from 'src/app/models/pharmacydocument';
import { Sale } from 'src/app/models/sale';
import { PharmacyDocumentService } from 'src/app/services/pharmacydocument.service';
import { SaleService } from 'src/app/services/sale.service';

@Component({
  selector: 'app-find-document',
  templateUrl: './find-document.component.html',
  styleUrls: ['./find-document.component.css']
})
export class FindDocumentComponent implements OnInit {
  
  myForm: FormGroup = new FormGroup({
    'dateDocument': new FormControl(''),
    'dateSell': new FormControl(''),
    'earn': new FormControl('')
  });
  documentation = false;
  soldmedicine = false;
  earnings = false;
  documents: PharmacyDocument [] = [];
  listOfsoldMedicines: Sale[] = [];
  totalEarnings: number = 0;

  constructor(
    private documentService: PharmacyDocumentService,
    private sellService: SaleService,
    private router: Router
  ) { }

  ngOnInit(): void {
  }

  onClose(){
    if( this.documentation === true ){
      this.documentation = false;
    } else if( this.soldmedicine === true ){
      this.soldmedicine = false;
    } else if( this.earnings === true ){
      this.earnings = false;
    }
  }

  findDocument(){
    this.documentation = true;
    this.documentService.getDocumentByDate(this.myForm.value['dateDocument']).subscribe(
      data =>{
        this.documents = data;
      }
    )
  }

  onAdd(idDocument: number, idPharmacy: number, idSupplier: number){
    this.router.navigate(['/adddelivered', idDocument, idPharmacy, idSupplier]);
  }

  findSellMedicine(){
    this.soldmedicine = true;
    this.sellService.getByDate(this.myForm.value['dateSell']).subscribe(
      data => {
        this.listOfsoldMedicines = data;
      }
    )
  }

  getEarnForDate(){
    this.earnings = true;
    this.sellService.getByDate(this.myForm.value['earn']).subscribe(
      data => {
        this.listOfsoldMedicines= data;
        for ( let i = 0; i < this.listOfsoldMedicines.length; i++ ){
          this.totalEarnings = this.totalEarnings + this.listOfsoldMedicines[i].amount * this.listOfsoldMedicines[i].price;
        }
      }
    )
  }

  viewDelivereds(id: number){
    this.router.navigate(['viewdelivered', id]);
  }
}
