import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Pharmacy } from 'src/app/models/pharmacy';
import { PharmacyService } from 'src/app/services/pharmacy.service';

@Component({
  selector: 'app-pharmacy-list',
  templateUrl: './pharmacy-list.component.html',
  styleUrls: ['./pharmacy-list.component.css']
})
export class PharmacyListComponent implements OnInit {

  pharmacylist: Pharmacy[] = [];

  constructor( private pharmacyService: PharmacyService,
               private router: Router
              ) { }

  ngOnInit(): void {
    this.getAll();
  }

  getAll(){
    this.pharmacyService.getAll().subscribe(
      data => {
        this.pharmacylist = data;
      }
    )
  }

  onUpdate(id : number){
    this.router.navigate(['/editpharmacy', id]);
  }

  onDelete(id: number){
    this.pharmacyService.deletePharmacy(id).subscribe(
      data =>{
        window.location.reload();
      }
    )
  }

  onView(id: number){
    this.router.navigate(['/pharmacyinventory', id]);
  }
}
