import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PharmacyUpdateComponent } from './pharmacy-update.component';

describe('PharmacyUpdateComponent', () => {
  let component: PharmacyUpdateComponent;
  let fixture: ComponentFixture<PharmacyUpdateComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PharmacyUpdateComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PharmacyUpdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
