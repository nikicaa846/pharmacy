import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PharmacyService } from 'src/app/services/pharmacy.service';

@Component({
  selector: 'app-pharmacy-update',
  templateUrl: './pharmacy-update.component.html',
  styleUrls: ['./pharmacy-update.component.css']
})
export class PharmacyUpdateComponent implements OnInit {

  pharmacyFormGroup: FormGroup = new FormGroup({
    'id': new FormControl(''),
    'name': new FormControl(''),
    'address' : new FormControl('')
  });
  id: number;

  constructor( private route: ActivatedRoute,
               private pharmacyService: PharmacyService, 
               private router: Router
              ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.pharmacyService.getPharmacyById(this.id).subscribe(
      data => {
        this.pharmacyFormGroup = new FormGroup({
          'id': new FormControl(data.idPharmacy),
          'name': new FormControl(data.name),
          'address' : new FormControl(data.address)
        })
      }
    );
  }


  update(){
    const pharmacy = this.pharmacyFormGroup.value;
    this.pharmacyService.updatePharmacy(this.id, pharmacy ).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    )
    this.router.navigate(['/pharmacy']);
  }
}
