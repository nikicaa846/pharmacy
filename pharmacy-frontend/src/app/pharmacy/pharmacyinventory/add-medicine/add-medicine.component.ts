import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PharmacyInventoryService } from 'src/app/services/pharmacyinventory.service';
import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-add-medicine',
  templateUrl: './add-medicine.component.html',
  styleUrls: ['./add-medicine.component.css']
})
export class AddMedicineComponent implements OnInit {

  pharmacyInventoryFormGroup: FormGroup;
  id: number;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private pharmacyInventoryService: PharmacyInventoryService,
    private supplierService: SupplierService
  ) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];
    this.pharmacyInventoryFormGroup = new FormGroup({
      'idPharmacy': new FormControl(this.id),
      'idMedicine': new FormControl(''),
      'supplier' : new FormControl(''),
      'name' : new FormControl(''),
      'amount' : new FormControl(''),
      'price' : new FormControl('')
    });
  }

  add(){
   const medicine = this.pharmacyInventoryFormGroup.value;
    this.supplierService.getAll().subscribe(
      data => {
        const suppliers = data;
        if(suppliers.find(item => item.name === this.pharmacyInventoryFormGroup.value['supplier']) !== undefined){
          medicine.idSupplier = suppliers.find(item => item.name === this.pharmacyInventoryFormGroup.value['supplier']).idSupplier;
          this.pharmacyInventoryService.createPharmacyMedicine(medicine).subscribe(
            data => {
              window.location.reload();
            }
          );
        } else {
          window.alert("Supplier name which you input doesn't exsist in your base!");
        }
      }
    )
    this.router.navigate(['pharmacyinventory', this.id]);
  }
}
