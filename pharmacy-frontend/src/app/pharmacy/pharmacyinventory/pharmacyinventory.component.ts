import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { PharmacyInventory } from 'src/app/models/pharmacyinventory';
import { PharmacyInventoryService } from 'src/app/services/pharmacyinventory.service';

@Component({
  selector: 'app-pharmacyinventory',
  templateUrl: './pharmacyinventory.component.html',
  styleUrls: ['./pharmacyinventory.component.css']
})
export class PharmacyInventoryComponent implements OnInit {

  medicines: PharmacyInventory[] = [];

  constructor(
    private pharmacyInventoryService: PharmacyInventoryService, 
    private route: ActivatedRoute,
    private router: Router,
    ) { }

  ngOnInit(): void {
    const id = this.route.snapshot.params['id'];
    this.pharmacyInventoryService.findByPharmacyId(id).subscribe(
      data => {
        this.medicines = data;
      }
    )
  }

  onAdd(id: number){
    this.router.navigate(['/addpharmacymedicine', id]);
  }

  onUpdate(idPharmacy: number, idMedicine: number, idSupplier: number){
    this.router.navigate(['/editphramcymedicine', idPharmacy, idMedicine, idSupplier]);
  }

  onDelete(idPharmacy: number, idMedicine: number, idSupplier: number){
    this.pharmacyInventoryService.deletePharmacyMedicine(idPharmacy, idMedicine, idSupplier).subscribe(
      data => {
        window.location.reload();
      }
    )
  }

  onOrder(idSupplier: number, idMedicine: number){
    this.router.navigate(['addorders', idSupplier, idMedicine]);
  }

  addToShop(id: number){
    this.router.navigate(['sell', id]);
  }
}
