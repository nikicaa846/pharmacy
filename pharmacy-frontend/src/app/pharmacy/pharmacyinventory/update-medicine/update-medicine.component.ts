import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { PharmacyInventoryService } from 'src/app/services/pharmacyinventory.service';

@Component({
  selector: 'app-update-medicine',
  templateUrl: './update-medicine.component.html',
  styleUrls: ['./update-medicine.component.css']
})
export class UpdateMedicineComponent implements OnInit {

  pharmacyInventoryFormGroup: FormGroup = new FormGroup({
    'idPharmacy': new FormControl(''),
    'idMedicine': new FormControl(''),
    'idSupplier' : new FormControl(''),
    'name': new FormControl(''),
    'amount' : new FormControl(''),
    'price' : new FormControl('')
  });

  idPharmacy: number;
  idMedicine: number;
  idSupplier: number;

  constructor(
    private route: ActivatedRoute,
    private pharmacyInventoryService: PharmacyInventoryService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.idPharmacy = this.route.snapshot.params['idPharmacy'];
    this.idMedicine = this.route.snapshot.params['idMedicine'];
    this.idSupplier = this.route.snapshot.params['idSupplier'];
    this.pharmacyInventoryService.getPharmacyMedicineById(this.idPharmacy, this.idMedicine, this.idSupplier).subscribe(
      data => {
        this.pharmacyInventoryFormGroup = new FormGroup({
          'idPharmacy': new FormControl(data.idPharmacy),
          'idMedicine': new FormControl(data.idMedicine),
          'idSupplier' : new FormControl(data.idSupplier),
          'name' : new FormControl(data.name),
          'amount' : new FormControl(data.amount),
          'price' : new FormControl(data.price)
        });
      }
    )
  }


  update(){
    const medicine = this.pharmacyInventoryFormGroup.value;
    this.pharmacyInventoryService.updatePharmacyMedicine(this.idPharmacy, this.idMedicine , this.idSupplier, medicine)
    .subscribe(
      data =>{
        window.location.reload();
      }
    );
    this.router.navigate(['/pharmacyinventory', this.idPharmacy]);
  }
}
