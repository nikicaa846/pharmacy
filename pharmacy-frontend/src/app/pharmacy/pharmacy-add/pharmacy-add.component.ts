import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

import { PharmacyService } from 'src/app/services/pharmacy.service';

@Component({
  selector: 'app-pharmacy-add',
  templateUrl: './pharmacy-add.component.html',
  styleUrls: ['./pharmacy-add.component.css']
})
export class PharmacyAddComponent implements OnInit {

  pharmacyFormGroup: FormGroup;

  constructor(private pharmacyService: PharmacyService,
              private router: Router
              ) { }

  ngOnInit(): void {
    this.pharmacyFormGroup= new FormGroup({
      'name': new FormControl(''),
      'address' : new FormControl('')
    })
  }

  onAdd(){
    const pharmacy = this.pharmacyFormGroup.value;
    this.pharmacyService.addPharmacy(pharmacy).subscribe(
      data => {
        window.location.reload();
      },
      error => console.log(error)
    );
    this.router.navigate(['/pharmacy']);
  }
}
