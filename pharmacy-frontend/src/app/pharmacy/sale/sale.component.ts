import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { PharmacyInventory } from 'src/app/models/pharmacyinventory';
import { Sale } from 'src/app/models/sale';
import { PharmacyInventoryService } from 'src/app/services/pharmacyinventory.service';
import { SaleService } from 'src/app/services/sale.service';
import { SupplierService } from 'src/app/services/supplier.service';

@Component({
  selector: 'app-sale',
  templateUrl: './sale.component.html',
  styleUrls: ['./sale.component.css']
})
export class SaleComponent implements OnInit {

  saleFormGroup: FormGroup;
  idPharmacy: number;
  listOfSoldMedicines: Sale[] = [];
  bill: number = 0;
  click = false;

  constructor(
    private route: ActivatedRoute,
    private pharmacyInventoryService: PharmacyInventoryService,
    private sellService: SaleService,
    private supplierService: SupplierService
  ) { }

  ngOnInit(): void {
    this.saleFormGroup = new FormGroup({
      'idMedicine': new FormControl(''),
      'supplier': new FormControl(''),
      'amount': new FormControl('')
    });
    this.idPharmacy = this.route.snapshot.params['id'];
  }

  onAdd() {
    this.click = true;
    this.supplierService.getAll().subscribe(
      data => {
        const suppliers = data;
        if (suppliers.find(item => item.name === this.saleFormGroup.value['supplier']) !== undefined) {
          const idsupplier = suppliers.find(item => item.name === this.saleFormGroup.value['supplier']).idSupplier;
          this.pharmacyInventoryService.getPharmacyMedicineById(this.idPharmacy, this.saleFormGroup.value['idMedicine'], idsupplier)
            .subscribe(
              data => {
                const availableMedicine = data;
                if (availableMedicine.amount > this.saleFormGroup.value['amount']) {
                  const medicineForSell = 
                  new Sale(availableMedicine.price , this.saleFormGroup.value['amount'], availableMedicine);
                  this.listOfSoldMedicines.push(medicineForSell);
                  for (let i = 0; i < this.listOfSoldMedicines.length; i++) {
                    this.bill = this.bill + this.listOfSoldMedicines[i].amount * this.listOfSoldMedicines[i].price;
                  }
                } else {
                  window.alert("Pharmacy doensn't dispose with amount of medicine which you need.\nChoose another supplier.");
                }
              }
            );
        }
      }
    )
  }

  printBill(){
    for(let i = 0; i < this.listOfSoldMedicines.length; i++){
      this.pharmacyInventoryService.getPharmacyMedicineById
      (this.idPharmacy, this.listOfSoldMedicines[i].pharmacyInventory.idMedicine, this.listOfSoldMedicines[i].pharmacyInventory.idSupplier)
      .subscribe(
        data => {
          const availableMedicine = data;
          this.sellService.addSell(this.listOfSoldMedicines[i]).subscribe(
            data => {
              const soldProduct = 
              new PharmacyInventory(this.idPharmacy,this.listOfSoldMedicines[i].pharmacyInventory.idMedicine, this.listOfSoldMedicines[i].pharmacyInventory.idSupplier, 
                availableMedicine.name, this.listOfSoldMedicines[i].amount, availableMedicine.price );
                this.pharmacyInventoryService.
                sellMedicine
                (this.idPharmacy, this.listOfSoldMedicines[i].pharmacyInventory.idMedicine, this.listOfSoldMedicines[i].pharmacyInventory.idSupplier, soldProduct)
                .subscribe(
                  data => {
                    window.location.reload();
                  }
                )
            }
          )
        }
      )
    }
  }
}
