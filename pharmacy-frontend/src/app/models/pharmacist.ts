export class Pharmacist {
    jmbgworker: number;
    qualification: string;

    constructor( jmbgworker : number, qualification: string){
        this.jmbgworker = jmbgworker;
        this.qualification = qualification;
    }
}