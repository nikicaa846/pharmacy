export class Comercialist {
    jmbgworker: number;
    mobilePhone: number;

    constructor( jmbgworker: number, mobilePhone: number){
        this.jmbgworker = jmbgworker;
        this.mobilePhone = mobilePhone;
    }
}