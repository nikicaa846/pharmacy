import { Medicine } from './medicine';
import { Pharmacy } from './pharmacy';
import { Supplier } from './supplier';

export class Orders{
    idOrder: number;
    medicine: Medicine;
    supplier: Supplier;
    pharmacy: Pharmacy;
    amount: number;
    orderdate: Date;

    constructor(medicine: Medicine, supplier: Supplier, pharmacy: Pharmacy, amount: number){
        this.medicine = medicine;
        this.supplier = supplier;
        this.pharmacy = pharmacy;
        this.amount = amount;
    }
}