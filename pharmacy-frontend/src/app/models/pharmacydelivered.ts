import { Medicine } from './medicine';
import { PharmacyDocument } from './pharmacydocument';

export class PharmacyDelivered{
    idDelivered: number;
    pharmacyDocument: PharmacyDocument;
    medicine: Medicine;
    amount: number;

    constructor( pharmacyDoc : PharmacyDocument, medicine: Medicine, amount: number){
        this.pharmacyDocument = pharmacyDoc;
        this.medicine = medicine;
        this.amount = amount;
    }
}