import { PharmacyInventory } from './pharmacyinventory';

export class Sale{
    idSell: number;
    pharmacyInventory: PharmacyInventory;
    price: number;
    date: Date;
    amount: number;

    constructor(price: number, amount: number, pharmacyInventory: PharmacyInventory){
        this.price = price;
        this.amount = amount;
        this.pharmacyInventory = pharmacyInventory;
    }
}