export class PharmacyInventory{
    idPharmacy: number;
    idMedicine: number;
    idSupplier: number;
    name: string;
    amount: number;
    price: number;

    constructor(idp : number, idm: number, ids: number, name: string, amount: number, price: number){
        this.idPharmacy = idp;
        this.idMedicine = idm;
        this.idSupplier = ids;
        this.name = name;
        this.amount = amount;
        this.price = price;
    }
}