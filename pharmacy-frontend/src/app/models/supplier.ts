export class Supplier {
    idSupplier: number;
    name: string;
    address: string;
    phone: string;
}