import { Comercialist } from './comercialist';
import { Pharmacy } from './pharmacy';
import { Supplier } from './supplier';

export class PharmacyDocument{
    idDocument: number;
    datedelivered: Date;
    comercialist: Comercialist;
    supplier: Supplier;
    pharmacy: Pharmacy;

    constructor( date: Date, comercialist: Comercialist, supplier: Supplier, pharmacy: Pharmacy){
        this.datedelivered = date;
        this.comercialist = comercialist;
        this.supplier = supplier;
        this.pharmacy = pharmacy;
    }
}