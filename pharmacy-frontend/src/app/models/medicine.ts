export class Medicine {
    idMedicine: number;
    name: string;

    constructor(idmedicine: number, name: string){
        this.idMedicine = idmedicine;
        this.name = name;
    }
}