export class SupplierInventory{
    idSupplier: number;
    idMedicine: number;
    name: string;
    amount: number;
    price: number;

    constructor( idsupplier: number, idmedicine: number, amount: number ){
        this.idSupplier = idsupplier;
        this.idMedicine = idmedicine;
        this.amount = amount;
    }
}