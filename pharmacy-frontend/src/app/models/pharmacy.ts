export class Pharmacy {
    idPharmacy: number;
    name: string;
    address: string;
}