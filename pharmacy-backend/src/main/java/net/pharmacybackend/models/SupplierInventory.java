package net.pharmacybackend.models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "supplier_inventory")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@IdClass(SupplierInventoryID.class)
public class SupplierInventory {

	@Id
	private Long idSupplier;
	
	@Id
	private Integer idMedicine;
	
	private String name;
	
	private Integer amount;
	
	private double price;
}
