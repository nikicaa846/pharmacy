package net.pharmacybackend.models;

import java.time.LocalDate;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pharmacy_document")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
public class PharmacyDocument {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idDocument;

	private LocalDate datedelivered;
	
	@NotNull
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "jmbgworker", nullable = false)
	private Comercialist comercialist;

	@NotNull
	@ManyToOne
	@JoinColumn(name = "idSupplier", nullable = false)
	private Supplier supplier;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idPharmacy", nullable = false)
	private Pharmacy pharmacy;
	
	@JsonIgnore
	@NotNull
	@OneToMany(mappedBy = "pharmacyDocument")
	private Set<PharmacyDelivered> pharmacyDelivered;

}