package net.pharmacybackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "medicine")
public class Medicine {

	@Id
	private Integer idMedicine;
	
	private String name;
	
	@OneToMany(mappedBy = "medicine")
	@JsonIgnore
	private Set<PharmacyDelivered> pharmacyDelivered;
	
	@JsonIgnore
	@NotNull
	@OneToMany(mappedBy = "medicine")
	private Set<Order> order;
	
}
