package net.pharmacybackend.models;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
@Getter
@Setter
@Entity
@Table(name = "comercialist")
@IdClass(WorkerID.class)
public class Comercialist {
	
	
	@Id
    private Long jmbgworker;

	@Column(length = 15)
	private Long mobilePhone;

	@NotNull
	@OneToMany(mappedBy = "comercialist")
	@JsonIgnore
	private Set<PharmacyDocument> pharmacyDocument;
}
