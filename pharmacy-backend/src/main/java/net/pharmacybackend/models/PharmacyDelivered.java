package net.pharmacybackend.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pharmacy_delivered")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class PharmacyDelivered {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idDelivered;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idDocument", nullable = false)
	private PharmacyDocument pharmacyDocument;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idMedicine", nullable = false)
	private Medicine medicine;
	
	private Integer amount;
	
}
