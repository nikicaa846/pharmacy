package net.pharmacybackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table( name = "orders")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idOrder;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idMedicine", nullable = false)
	private Medicine medicine;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idSupplier", nullable = false)
	private Supplier supplier;
	
	@NotNull
	@ManyToOne
	@JoinColumn(name = "idPharmacy", nullable = false)
	private Pharmacy pharmacy;
	
	private Integer amount;
	
	private LocalDate orderdate;
}
