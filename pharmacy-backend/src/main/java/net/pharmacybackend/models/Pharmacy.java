package net.pharmacybackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pharmacy")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Pharmacy {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idPharmacy;

	private String name;

	private String address;

	@OneToMany(mappedBy = "pharmacy")
	@JsonIgnore
	private Set<Order> order;
	
	@OneToMany(mappedBy = "pharmacy")
	@JsonIgnore
	private Set<PharmacyDocument> pharmacyDocument;
}
