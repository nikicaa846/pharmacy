package net.pharmacybackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "supplier")
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Supplier {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idSupplier;

	private String name;

	private String address;

	private String phone;

	@JsonIgnore
	@NotNull
	@OneToMany(mappedBy = "supplier")
	private Set<PharmacyDocument> pharmacyDocument;
	
	@JsonIgnore
	@NotNull
	@OneToMany(mappedBy = "supplier")
	private Set<Order> order;
}

