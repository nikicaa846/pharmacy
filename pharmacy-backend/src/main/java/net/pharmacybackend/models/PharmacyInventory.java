package net.pharmacybackend.models;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sun.istack.NotNull;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "pharmacy_inventory")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(PharmacyInventoryID.class)
public class PharmacyInventory {

	@Id
	private Long idPharmacy;
	
	@Id
	private Integer idMedicine;
	
	@Id
	private Long idSupplier;
	
	private String name;
	
	private Integer amount;
	
	private Double price;
	
	@OneToMany(mappedBy = "pharmacyInventory")
	@JsonIgnore
	@NotNull
	private Set<Sale> sale;
}
