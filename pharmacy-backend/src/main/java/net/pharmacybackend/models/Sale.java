package net.pharmacybackend.models;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "sale")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Sale {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idSale;
	
	private Double price;
	
	private LocalDate date;
	
	private Integer amount;
	
	@ManyToOne
	@JoinColumns({
			@JoinColumn(name = "idPharmacy", nullable = false),
			@JoinColumn(name = "idMedicine", nullable = false),
			@JoinColumn(name = "idSupplier", nullable = false)
	})
	private PharmacyInventory pharmacyInventory;
}
