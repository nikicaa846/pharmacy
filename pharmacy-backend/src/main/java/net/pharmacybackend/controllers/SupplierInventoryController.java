package net.pharmacybackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.SupplierInventory;
import net.pharmacybackend.services.SupplierInventoryService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/supplierinventory")
public class SupplierInventoryController {

	@Autowired
	private SupplierInventoryService supplierInventoryService;
	
	@GetMapping
	public List<SupplierInventory> findByIdSupplier(@RequestParam Long idSupplier){
		return this.supplierInventoryService.findByIdSupplier(idSupplier);
	}
	
	@GetMapping("/supplier/{idsupplier}/medicine/{idmedicine}")
	public SupplierInventory getMedicineById(@PathVariable Long idsupplier, @PathVariable Integer idmedicine) {
		return this.supplierInventoryService.getMedicineById(idsupplier, idmedicine);
	}
	
	@PostMapping
	public SupplierInventory addMedicine(@RequestBody SupplierInventory medicine) {
		return this.supplierInventoryService.addMedicine(medicine);
	}
	
	@PutMapping("/supplier/{idsupplier}/medicine/{idmedicine}")
	public ResponseEntity<SupplierInventory> 
		updateMedicine(@PathVariable Long idsupplier, @PathVariable Integer idmedicine,@RequestBody SupplierInventory medicine){
		return ResponseEntity.ok(this.supplierInventoryService.updateMedicine(idsupplier, idmedicine, medicine));
	}
	
	@PutMapping("/sell/supplier/{idsupplier}/medicine/{idmedicine}")
	public ResponseEntity<SupplierInventory> 
		sellMedicine(@PathVariable Long idsupplier, @PathVariable Integer idmedicine,@RequestBody SupplierInventory medicine){
		return ResponseEntity.ok(this.supplierInventoryService.sellMedicine(idsupplier, idmedicine, medicine));
	}
	
	@DeleteMapping("/supplier/{idsupplier}/medicine/{idmedicine}")
	public void deleteMedicine(@PathVariable Long idsupplier, @PathVariable Integer idmedicine) {
		this.supplierInventoryService.deleteMedicine(idsupplier, idmedicine);
	}
}
