package net.pharmacybackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Supplier;
import net.pharmacybackend.services.SupplierService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/supplier")
public class SupplierController {

	@Autowired
	private SupplierService supplierService;
	
	@GetMapping
	public List<Supplier> getAll(){
		return this.supplierService.getAll();
	}
	
	@GetMapping("/{id}")
	public Supplier getSupplier(@PathVariable Long id) {
		return this.supplierService.getSupplier(id);
	}
	
	@PostMapping
	public Supplier addSupplier(@RequestBody Supplier supplier) {
		return this.supplierService.addSupplier(supplier);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Supplier> updateSupplier(@PathVariable Long id, @RequestBody Supplier supplier){
		return ResponseEntity.ok(this.supplierService.updateSupplier(id, supplier));
	}
	
	@DeleteMapping("/{id}")
	public void deleteSupplier(@PathVariable Long id) {
		this.supplierService.deleteSupplier(id);
	}
}
