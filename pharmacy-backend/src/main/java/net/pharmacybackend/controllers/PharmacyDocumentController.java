package net.pharmacybackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.PharmacyDocument;
import net.pharmacybackend.services.PharmacyDocumentService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/pharmacydocument")
public class PharmacyDocumentController {

	@Autowired
	private PharmacyDocumentService pharmacyDocumentService;
	
	@GetMapping("/{id}")
	public PharmacyDocument getDocument(@PathVariable Long id){
		return this.pharmacyDocumentService.getDocument(id);
	}
	
	@GetMapping
	public List<PharmacyDocument> getDocuments(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")LocalDate date){
		return this.pharmacyDocumentService.findDocuments(date);
	}

	
	@PostMapping
	public PharmacyDocument addDocument(@RequestBody PharmacyDocument document) {
		return this.pharmacyDocumentService.addDocument(document);
	}
}
