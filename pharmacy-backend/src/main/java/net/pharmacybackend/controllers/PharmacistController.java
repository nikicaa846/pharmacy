package net.pharmacybackend.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Pharmacist;
import net.pharmacybackend.services.PharmacistService;


@RestController
@CrossOrigin(origins = "http://localhost:4200", allowedHeaders = "*")
@RequestMapping(value = "/api/v1/pharmacist")
public class PharmacistController {

	@Autowired
	private PharmacistService pharmacistService;
	
	@GetMapping("/{id}")
	public Pharmacist getByIdPharmacist(@PathVariable Long id) {
		return this.pharmacistService.getById(id);
	}
	
	@PostMapping
	public Pharmacist createPharmacist(@RequestBody Pharmacist pharmacist) {
		return this.pharmacistService.addPharmacist(pharmacist);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Pharmacist> updatePharmacist(@PathVariable Long id, @RequestBody Pharmacist pharmacist){
		return ResponseEntity.ok(this.pharmacistService.updatePharmacist(id, pharmacist));
	}
}
