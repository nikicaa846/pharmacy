package net.pharmacybackend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Medicine;
import net.pharmacybackend.services.MedicineService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/medicine")
public class MedicineController {

	@Autowired
	private MedicineService medicineService;
	
	@GetMapping("/{id}")
	public Medicine getMedicine(@PathVariable Integer id) {
		return this.medicineService.getMedicineById(id);
	}
	
	@PostMapping
	public Medicine addMedicine(@RequestBody Medicine medicine) {
		return this.medicineService.addMedicine(medicine);
	}
}
