package net.pharmacybackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Order;
import net.pharmacybackend.services.OrderService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/orders")
public class OrderController {

	@Autowired
	private OrderService orderService;
	
	@GetMapping("/{idorder}")
	public Order getOrderById(@PathVariable(required = false, value = "idorder") Long id) {
		return this.orderService.getOrderById(id);
	}
	
	@GetMapping
	public List<Order> getByDate(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd")  LocalDate date) {
		return this.orderService.getOrderByDate(date);
	}
	
	@PostMapping
	public Order addOrder(@RequestBody Order order) {
		return this.orderService.addOrder(order);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Order> updateOrder(@PathVariable Long id, @RequestBody Order order){
		return ResponseEntity.ok(this.orderService.updateOrder(id, order));
	}
	
	@DeleteMapping("/{id}")
	public void deleteOrder(@PathVariable Long id) {
		this.orderService.deleteOrder(id);
	}
}
