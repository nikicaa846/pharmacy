package net.pharmacybackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Pharmacy;
import net.pharmacybackend.services.PharmacyService;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/api/v1/pharmacy")
public class PharmacyController {

	@Autowired
	private PharmacyService pharmacyService;
	
	@GetMapping
	public List<Pharmacy> getAllPharmacies(){
		return this.pharmacyService.getAllPharmacies();
	}
	
	@GetMapping("/{id}")
	public Pharmacy getPharmacyById(@PathVariable Long id) {
		return this.pharmacyService.getPharmacyById(id);
	}
	
	@PostMapping
	public Pharmacy addPharmacy(@RequestBody Pharmacy pharmacy) {
		return this.pharmacyService.addPharmacy(pharmacy);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Pharmacy> updatePharmacy(@PathVariable Long id, @RequestBody Pharmacy pharmacy){
		return ResponseEntity.ok(this.pharmacyService.updatePharmacy(id, pharmacy));
	}
	
	@DeleteMapping("/{id}")
	public void deletePharmacy(@PathVariable Long id) {
		this.pharmacyService.deletePharmacy(id);
	}
}
