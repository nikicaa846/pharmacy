package net.pharmacybackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.PharmacyInventory;
import net.pharmacybackend.services.PharmacyInventoryService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/pharmacyinventory")
public class PharmacyInventoryController {

	@Autowired
	private PharmacyInventoryService pharmacyInventoryService;
	
	@GetMapping("/pharmacy/{idpharmacy}/medicine/{idmedicine}/supplier/{idsupplier}")
	public PharmacyInventory getById(@PathVariable Long idpharmacy, @PathVariable Integer idmedicine, @PathVariable Long idsupplier) {
		return this.pharmacyInventoryService.getById(idpharmacy, idmedicine, idsupplier);
	}
	
	@GetMapping
	public List<PharmacyInventory> findByPharmacyId(@RequestParam Long idPharmacy){
		return this.pharmacyInventoryService.findByPharmacyId(idPharmacy);
	}
	
	@PostMapping
	public PharmacyInventory addMedicine(@RequestBody PharmacyInventory medicine) {
		return this.pharmacyInventoryService.addMedicine(medicine);
	}
	
	@PutMapping("/pharmacy/{idpharmacy}/medicine/{idmedicine}/supplier/{idsupplier}")
	public ResponseEntity<PharmacyInventory> 
		updateMedicine(@PathVariable Long idpharmacy, @PathVariable Integer idmedicine,@PathVariable Long idsupplier, @RequestBody PharmacyInventory medicine){
		return ResponseEntity.ok(this.pharmacyInventoryService.updateMedicine(idpharmacy, idmedicine,idsupplier, medicine));
	}
	
	@PutMapping("/sell/pharmacy/{idpharmacy}/medicine/{idmedicine}/supplier/{idsupplier}")
	public ResponseEntity<PharmacyInventory>
		sellMedicine(@PathVariable Long idpharmacy, @PathVariable Integer idmedicine,@PathVariable Long idsupplier, @RequestBody PharmacyInventory medicine){
		return ResponseEntity.ok(this.pharmacyInventoryService.sellMedicine(idpharmacy, idmedicine, idsupplier, medicine));
	}
	
	@PutMapping("/delivered/pharmacy/{idpharmacy}/medicine/{idmedicine}/supplier/{idsupplier}")
	public ResponseEntity<PharmacyInventory>
		deliveredMedicine(@PathVariable Long idpharmacy, @PathVariable Integer idmedicine,@PathVariable Long idsupplier, @RequestBody PharmacyInventory medicine){
		return ResponseEntity.ok(this.pharmacyInventoryService.deliveredMedicine(idpharmacy, idmedicine, idsupplier, medicine));
	}
	
	@DeleteMapping("/{idpharmacy}/{idmedicine}/{idsupplier}")
	public void deleteMedicine(@PathVariable Long idpharmacy, @PathVariable Integer idmedicine, @PathVariable Long idsupplier) {
		this.pharmacyInventoryService.deleteMedicine(idpharmacy, idmedicine, idsupplier);
	}
}
