package net.pharmacybackend.controllers;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Sale;
import net.pharmacybackend.services.SaleService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/sell")
public class SaleController {

	@Autowired
	private SaleService saleService;

	@GetMapping
	public List<Sale> findByDate( @RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date){
		return this.saleService.findSellsByDate(date);
	}

	@PostMapping
	public Sale addSell(@RequestBody Sale sell) {
		return this.saleService.addSell(sell);
	}
}
