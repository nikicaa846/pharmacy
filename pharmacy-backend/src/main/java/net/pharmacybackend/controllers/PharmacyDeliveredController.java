package net.pharmacybackend.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.PharmacyDelivered;
import net.pharmacybackend.services.PharmacyDeliveredService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/pharmacydelivered")
public class PharmacyDeliveredController {

	@Autowired
	private PharmacyDeliveredService pharmacyDeliveredService;
	
	@GetMapping
	public List<PharmacyDelivered> findById(@RequestParam Long idDocument){
		return this.pharmacyDeliveredService.findDeliveredsByIdDocument(idDocument);
	}
	
	@PostMapping
	public PharmacyDelivered addDelivery(@RequestBody PharmacyDelivered delivery) {
		return this.pharmacyDeliveredService.addDelivered(delivery);
	}
}
