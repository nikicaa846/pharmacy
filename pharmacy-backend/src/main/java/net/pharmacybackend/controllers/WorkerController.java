package net.pharmacybackend.controllers;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Worker;
import net.pharmacybackend.services.WorkerService;

@RestController
@CrossOrigin(origins = "http://localhost:4200/", allowedHeaders = "*")
@RequestMapping("/api/v1/worker")
public class WorkerController {

	@Autowired
	private WorkerService workService;
	
	
	@GetMapping
	public List<Worker> getAll(){
		return this.workService.getAllWorkers();
	}
	
	@GetMapping("/{id}")
	public Worker getWorkerById(@PathVariable Long id) {
		return this.workService.getWorkerById(id);
	}
	
	@PostMapping
	public Worker createWorker(@RequestBody Worker worker){
		return this.workService.addWorker(worker);
	}
	
	@PutMapping("/{id}")
	public ResponseEntity<Worker> updateWorker(@PathVariable Long id, @RequestBody Worker worker){
		return ResponseEntity.ok(this.workService.upadteWorker(id, worker));
	}
}
