package net.pharmacybackend.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import net.pharmacybackend.models.Comercialist;
import net.pharmacybackend.services.ComercialistService;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping("/api/v1/comercialist")
public class ComercialistController {

	@Autowired
	private ComercialistService comercialistService;

	@GetMapping("/{id}")
	public Comercialist getById(@PathVariable Long  id) {
		return this.comercialistService.getById(id);
	}

	@PostMapping
	public Comercialist createComercialist(@RequestBody Comercialist comercialist) {
		return this.comercialistService.addComercialist(comercialist);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Comercialist> updateComercialist(@PathVariable Long  id,
			@RequestBody Comercialist comercialist) {
		return ResponseEntity.ok(this.comercialistService.updateComercialist(id, comercialist));
	}
}
