package net.pharmacybackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.Pharmacist;
import net.pharmacybackend.models.WorkerID;

@Repository
public interface PharmacistRepository extends JpaRepository<Pharmacist, WorkerID> {
}
