package net.pharmacybackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.Supplier;

@Repository
public interface SupplierRepository extends JpaRepository<Supplier, Long> {

}
