package net.pharmacybackend.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.Sale;

@Repository
public interface SaleRepository extends JpaRepository<Sale, Integer> {

	List<Sale> findByDate(LocalDate date);
}
