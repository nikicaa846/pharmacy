package net.pharmacybackend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.SupplierInventory;
import net.pharmacybackend.models.SupplierInventoryID;

@Repository
public interface SupplierInventoryRepository extends JpaRepository<SupplierInventory, SupplierInventoryID>{

	List<SupplierInventory> findByIdSupplier(Long idSupplier);
}
