package net.pharmacybackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.Comercialist;
import net.pharmacybackend.models.WorkerID;


@Repository
public interface ComercialistRepository extends JpaRepository<Comercialist, WorkerID> {

}