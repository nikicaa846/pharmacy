package net.pharmacybackend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.PharmacyDelivered;

@Repository
public interface PharmacyDeliveredRepository extends JpaRepository<PharmacyDelivered, Integer> {

	List<PharmacyDelivered> findByPharmacyDocumentIdDocument(Long idDocument);
}
