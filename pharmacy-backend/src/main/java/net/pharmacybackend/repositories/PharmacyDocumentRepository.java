package net.pharmacybackend.repositories;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.PharmacyDocument;

@Repository
public interface PharmacyDocumentRepository extends JpaRepository<PharmacyDocument, Long>{
	
	List<PharmacyDocument> findByDatedelivered(LocalDate datedelivered);
}
