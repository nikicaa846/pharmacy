package net.pharmacybackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.Medicine;

@Repository
public interface MedicineRepository extends JpaRepository<Medicine, Integer> {

}

