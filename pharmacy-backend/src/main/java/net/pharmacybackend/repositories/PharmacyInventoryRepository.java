package net.pharmacybackend.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.PharmacyInventory;
import net.pharmacybackend.models.PharmacyInventoryID;

@Repository
public interface PharmacyInventoryRepository extends JpaRepository<PharmacyInventory, PharmacyInventoryID> {

	List<PharmacyInventory> findByIdPharmacy(Long idPharmacy);
}
