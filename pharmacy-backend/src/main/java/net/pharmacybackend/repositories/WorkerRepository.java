package net.pharmacybackend.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import net.pharmacybackend.models.Worker;
import net.pharmacybackend.models.WorkerID;

@Repository
public interface WorkerRepository extends JpaRepository<Worker, WorkerID> {
}
