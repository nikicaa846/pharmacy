package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.models.PharmacyDelivered;
import net.pharmacybackend.repositories.PharmacyDeliveredRepository;

@Service
public class PharmacyDeliveredService {

	@Autowired
	private PharmacyDeliveredRepository pharmacydeliveredRepo;
	
	public List<PharmacyDelivered> getAllDelivereds(){
		return this.pharmacydeliveredRepo.findAll();
	}

	public List<PharmacyDelivered> findDeliveredsByIdDocument(Long idDocument){
		return this.pharmacydeliveredRepo.findByPharmacyDocumentIdDocument(idDocument);
	}
	
	public PharmacyDelivered addDelivered(PharmacyDelivered delivery) {
		return this.pharmacydeliveredRepo.save(delivery);
	}
}
