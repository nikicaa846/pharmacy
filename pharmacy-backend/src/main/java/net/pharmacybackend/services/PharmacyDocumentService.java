package net.pharmacybackend.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.PharmacyDocument;
import net.pharmacybackend.repositories.PharmacyDocumentRepository;


@Service
public class PharmacyDocumentService {

	@Autowired
	private PharmacyDocumentRepository pharmacyDocumentRepo;
	
	public PharmacyDocument getDocument(Long id) {
		return this.pharmacyDocumentRepo.findById(id)
				.orElseThrow(() 
						-> new ResourceNotFoundException(
								"Document doesn't exist with id = " + id));
	}
	
	public List<PharmacyDocument> findDocuments(LocalDate date) {
		return this.pharmacyDocumentRepo.findByDatedelivered(date);
	}
	
	public PharmacyDocument addDocument(PharmacyDocument document) {
		return this.pharmacyDocumentRepo.save(document);
	}
}
