package net.pharmacybackend.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Medicine;
import net.pharmacybackend.repositories.MedicineRepository;

@Service
public class MedicineService {

	@Autowired
	private MedicineRepository medicineRepo;
	
	public Medicine getMedicineById(Integer idmedicine) {
		return this.medicineRepo.findById(idmedicine)
				.orElseThrow(() -> new ResourceNotFoundException("Medicine doesn't exist with id = " + idmedicine));
	}
	
	public Medicine addMedicine(Medicine medicine) {
		return this.medicineRepo.save(medicine);
	}
}
