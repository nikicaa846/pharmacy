package net.pharmacybackend.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Order;
import net.pharmacybackend.repositories.OrderRepository;

@Service
public class OrderService {

	@Autowired
	private OrderRepository orderRepo;
	
	public Order getOrderById(Long id) {
		return this.orderRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Order with id " + id + "doesn't exist"));
	}

	public List<Order> getOrderByDate(LocalDate orderdate) {
		 return this.orderRepo.findByOrderdate(orderdate);
	}
	
	public Order addOrder(Order order) {
		order.setOrderdate(LocalDate.now());
		return this.orderRepo.save(order);
	}
	
	public Order updateOrder(Long id, Order order){
		Order orderForUpdate = this.orderRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Order with id " + id + "doesn't exist"));
		
		orderForUpdate.setAmount(order.getAmount());
		orderForUpdate.setOrderdate(LocalDate.now());
		
		return this.orderRepo.save(orderForUpdate);
	}
	
	public void deleteOrder(Long id) {
		this.orderRepo.deleteById(id);
	}
}
