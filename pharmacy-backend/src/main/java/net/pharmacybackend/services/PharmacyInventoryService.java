package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.PharmacyInventory;
import net.pharmacybackend.models.PharmacyInventoryID;
import net.pharmacybackend.repositories.PharmacyInventoryRepository;

@Service
public class PharmacyInventoryService {

	@Autowired
	private PharmacyInventoryRepository pharmacyInventoryRepo;
	
	public List<PharmacyInventory> findByPharmacyId(Long idPharmacy){
		return this.pharmacyInventoryRepo.findByIdPharmacy(idPharmacy);
	}
	
	public PharmacyInventory getById(Long idpharmacy, Integer idmedicine, Long idsupplier){
		return this.pharmacyInventoryRepo.findById(new PharmacyInventoryID(idpharmacy, idmedicine, idsupplier))
				.orElseThrow(() 
						-> new ResourceNotFoundException(
								"Pharmacy " + idpharmacy + " doesn't contains medicine with id " + idmedicine));
	}
	
	public PharmacyInventory addMedicine(PharmacyInventory medicine) {
		return this.pharmacyInventoryRepo.save(medicine);
	}
	
	public PharmacyInventory updateMedicine( Long idpharmacy, Integer idmedicine, Long idsupplier, PharmacyInventory medicine ){
		PharmacyInventory medicineForUpdate = this.pharmacyInventoryRepo.findById(new PharmacyInventoryID(idpharmacy, idmedicine, idsupplier))
		.orElseThrow(() 
				-> new ResourceNotFoundException(
						"Pharmacy " + idpharmacy + " doesn't contains medicine with id " + idmedicine));
		
		medicineForUpdate.setIdPharmacy(medicine.getIdPharmacy());
		medicineForUpdate.setIdMedicine(medicine.getIdMedicine());
		medicineForUpdate.setName(medicine.getName());
		medicineForUpdate.setAmount(medicine.getAmount());
		medicineForUpdate.setPrice(medicine.getPrice());
		
		
		return this.pharmacyInventoryRepo.save(medicineForUpdate);
	}
	
	public int getAmountForSellMedicine( PharmacyInventory medicine1, PharmacyInventory medicine2) {
		return medicine1.getAmount() - medicine2.getAmount();
	}
	
	public PharmacyInventory sellMedicine(Long idpharmacy, Integer idmedicine, Long idsupplier, PharmacyInventory medicine){
		PharmacyInventory medicineForSell = this.getById(idpharmacy, idmedicine, idsupplier);
		
		medicineForSell.setAmount(getAmountForSellMedicine(medicineForSell, medicine));
		
		
		return this.addMedicine(medicineForSell);
	}

	public int getAmountForDelivered(PharmacyInventory medicine1, PharmacyInventory medicine2 ) {
		return medicine1.getAmount() + medicine2.getAmount();
	}
	
	public PharmacyInventory deliveredMedicine(Long idpharmacy, Integer idmedicine, Long idsupplier, PharmacyInventory medicine){
		PharmacyInventory medicineForUpdate = this.getById(idpharmacy, idmedicine, idsupplier);
		
		medicineForUpdate.setAmount(getAmountForDelivered(medicineForUpdate, medicine));
		
		return this.addMedicine(medicineForUpdate);
	}
	
	public void deleteMedicine(Long idpharmacy, Integer idmedicine, Long idsupplier) {
		this.pharmacyInventoryRepo.deleteById(new PharmacyInventoryID(idpharmacy, idmedicine, idsupplier));
	}
}
