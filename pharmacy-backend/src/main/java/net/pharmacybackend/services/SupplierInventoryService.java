package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.SupplierInventory;
import net.pharmacybackend.models.SupplierInventoryID;
import net.pharmacybackend.repositories.SupplierInventoryRepository;

@Service
public class SupplierInventoryService {

	@Autowired
	private SupplierInventoryRepository supplierInventoryRepo;
	
	public List<SupplierInventory> findByIdSupplier(Long idSupplier) {
		return this.supplierInventoryRepo.findByIdSupplier(idSupplier);
	}
	
	public SupplierInventory getMedicineById(Long idsupplier, Integer idmedicine) {
		return this.supplierInventoryRepo.findById(new SupplierInventoryID(idsupplier, idmedicine))
				.orElseThrow(() 
						-> new ResourceNotFoundException(
								"Supplier " + idsupplier + " doesn't contains medicine with id " + idmedicine));
	}
	
	public SupplierInventory addMedicine(SupplierInventory medicine) {
		return this.supplierInventoryRepo.save(medicine);
	}
	
	public SupplierInventory updateMedicine(Long idsupplier, Integer idmedicine, SupplierInventory medicine){
		SupplierInventory medicineForUpdate = this.getMedicineById(idsupplier, idmedicine);
		
		medicineForUpdate.setIdSupplier(medicine.getIdSupplier());
		medicineForUpdate.setIdMedicine(medicine.getIdMedicine());
		medicineForUpdate.setName(medicine.getName());
		medicineForUpdate.setAmount(medicine.getAmount());
		medicineForUpdate.setPrice(medicine.getPrice());
		
		return this.supplierInventoryRepo.save(medicineForUpdate);
	}
	
	public Integer getNewAmountForContainsMedicine(SupplierInventory medicine1, SupplierInventory medicine2) {
		return medicine1.getAmount() - medicine2.getAmount();
	}
	
	public SupplierInventory sellMedicine(Long idsupplier, Integer idmedicine, SupplierInventory medicine){
		SupplierInventory medicineForSell = this.getMedicineById(idsupplier, idmedicine);
	
		medicineForSell.setAmount(getNewAmountForContainsMedicine(medicineForSell, medicine));
		
		return this.addMedicine(medicineForSell);
	}
	
	public void deleteMedicine(Long idsupplier, Integer idmedicine) {
		this.supplierInventoryRepo.deleteById(new SupplierInventoryID(idsupplier, idmedicine));
	}
}
