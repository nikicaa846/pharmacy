package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Pharmacy;
import net.pharmacybackend.repositories.PharmacyRepository;

@Service
public class PharmacyService {

	@Autowired
	private PharmacyRepository pharmacyRepo;
	
	public List<Pharmacy> getAllPharmacies() {
		return this.pharmacyRepo.findAll();
	}
	
	public Pharmacy getPharmacyById(Long id) {
		return this.pharmacyRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Pharmacy doesn't exist with id = " + id));
	}
	
	public Pharmacy addPharmacy( Pharmacy pharmacy ) {
		return this.pharmacyRepo.save(pharmacy);
	}
	
	public Pharmacy updatePharmacy( Long id, Pharmacy pharmacy){
		Pharmacy updatePharmacy = this.getPharmacyById(id);
		
		updatePharmacy.setName(pharmacy.getName());
		updatePharmacy.setAddress(pharmacy.getAddress());

		return this.pharmacyRepo.save(updatePharmacy);
	}
	
	public void deletePharmacy( Long id) {
		this.pharmacyRepo.deleteById(id);
	}
}
