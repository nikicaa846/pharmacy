package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Comercialist;
import net.pharmacybackend.models.Worker;
import net.pharmacybackend.models.WorkerID;
import net.pharmacybackend.repositories.ComercialistRepository;

@Service
public class ComercialistService {

	@Autowired
	private ComercialistRepository comercialistRepo;

	@Autowired
	private WorkerService workerService;

	public Comercialist getById(Long id) {
		return this.comercialistRepo.findById(new WorkerID(id))
				.orElseThrow(() -> new ResourceNotFoundException("Comercialist doesn't exist with id = " + id));
	}

	public Comercialist addComercialist(Comercialist comercialist) {
		List<Worker> workers = this.workerService.getAllWorkers();
		for ( Worker worker: workers) {
			if( worker.getJmbgworker() == comercialist.getJmbgworker()) {
				comercialist.setJmbgworker(worker.getJmbgworker());
			}
		}
		return this.comercialistRepo.save(comercialist);
	}

	public Comercialist updateComercialist(Long id, Comercialist comercialist) {
		Comercialist updateComercialist = this.comercialistRepo.findById(new WorkerID(id))
				.orElseThrow(() -> new ResourceNotFoundException("Comercialist doesn't exist with id = " + id));
		
		updateComercialist.setJmbgworker(comercialist.getJmbgworker());
		updateComercialist.setMobilePhone(comercialist.getMobilePhone());


		return this.comercialistRepo.save(updateComercialist);
	}
}
