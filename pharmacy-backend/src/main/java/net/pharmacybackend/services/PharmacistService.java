package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Pharmacist;
import net.pharmacybackend.models.Worker;
import net.pharmacybackend.models.WorkerID;
import net.pharmacybackend.repositories.PharmacistRepository;

@Service
public class PharmacistService {

	@Autowired
	private PharmacistRepository pharmacistRepo;
	
	@Autowired
	private WorkerService workerService;


	public Pharmacist getById(Long id) {
		return this.pharmacistRepo.findById(new WorkerID(id))
				.orElseThrow(() -> new ResourceNotFoundException("Pharmacist doesn't exist with id = " + id));
	}
	
	public Pharmacist addPharmacist(Pharmacist pharmacist) {
		List<Worker> workers = this.workerService.getAllWorkers();
		for ( Worker worker: workers) {
			if( worker.getJmbgworker() == pharmacist.getJmbgworker()) {
				pharmacist.setJmbgworker(worker.getJmbgworker());
			}
		}
		return this.pharmacistRepo.save(pharmacist);	
	}
	
	public Pharmacist updatePharmacist(Long id, Pharmacist pharmacist){
		Pharmacist updatePharmacist = this.pharmacistRepo.findById(new WorkerID(id))
				.orElseThrow(() -> new ResourceNotFoundException("Pharmacist with id " + id + "doesn't exist"));
		updatePharmacist.setJmbgworker(pharmacist.getJmbgworker());
		updatePharmacist.setQualification(pharmacist.getQualification());
		
		return this.pharmacistRepo.save(updatePharmacist);
	}
	
}
