package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Worker;
import net.pharmacybackend.models.WorkerID;
import net.pharmacybackend.repositories.WorkerRepository;

@Service
public class WorkerService {

	@Autowired
	private WorkerRepository workerRepo;
    
	public List<Worker> getAllWorkers(){
		return this.workerRepo.findAll();
	}
	
	
	public Worker getWorkerById(Long id) {
		return this.workerRepo.findById(new WorkerID(id))
				.orElseThrow(() -> new ResourceNotFoundException("Worker with id " + id + "doesn't exist"));
	}
	
	public Worker addWorker(Worker worker) {
		return workerRepo.save(worker);
	}
	
	public Worker upadteWorker(Long id, Worker worker) {
		Worker updateWorker = this.workerRepo.findById(new WorkerID(id))
				.orElseThrow(() -> new ResourceNotFoundException("Worker with id " + id + "doesn't exist"));
		updateWorker.setName(worker.getName());
		updateWorker.setAddress(worker.getAddress());
		updateWorker.setSurname(worker.getSurname());
		
		return workerRepo.save(updateWorker);
	}
}
