package net.pharmacybackend.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.exception.ResourceNotFoundException;
import net.pharmacybackend.models.Supplier;
import net.pharmacybackend.repositories.SupplierRepository;

@Service
public class SupplierService {

	@Autowired
	private SupplierRepository supplierRepo;
	
	public List<Supplier> getAll() {
		return this.supplierRepo.findAll();
	}
	
	public Supplier getSupplier(Long id) {
		return this.supplierRepo.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("Pharmacy doesn't exist with id = " + id));
	}
	
	public Supplier addSupplier( Supplier supplier) {
		return this.supplierRepo.save(supplier);
	}
	
	public Supplier updateSupplier(Long id, Supplier supplier){
		Supplier updateSupplier = this.getSupplier(id);
		
		updateSupplier.setName(supplier.getName());
		updateSupplier.setAddress(supplier.getAddress());
		updateSupplier.setPharmacyDocument(supplier.getPharmacyDocument());
		updateSupplier.setPhone(supplier.getPhone());
		
		return this.supplierRepo.save(updateSupplier);
	}
	
	public void deleteSupplier(Long id ) {
		this.supplierRepo.deleteById(id);
	}
}
