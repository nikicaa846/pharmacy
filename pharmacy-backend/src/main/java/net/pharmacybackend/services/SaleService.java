package net.pharmacybackend.services;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.pharmacybackend.models.Sale;
import net.pharmacybackend.repositories.SaleRepository;

@Service
public class SaleService {

	@Autowired
	private SaleRepository saleRepo;
	
	
	public List<Sale> findSellsByDate(LocalDate date){
		return this.saleRepo.findByDate(date);
	}
	
	public Sale addSell(Sale sell) {
		sell.setDate(LocalDate.now());
		return this.saleRepo.save(sell);
	}
}
